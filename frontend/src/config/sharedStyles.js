export function siteMaxWidth() {
  return 1920;
}

export function appBarHeight(theme) {
  return theme.spacing(8);
}
