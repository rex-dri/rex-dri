/**
 * Function to generate a range of numbers from 0 to n-1
 *
 * @export
 * @param {number} n
 * @returns
 */
export default function range(n) {
  return [...Array(n).keys()];
}
