export default function getModerationTooltipAndClass(
  hasPendingModeration,
  userCanEdit
) {
  if (!userCanEdit) {
    return {
      moderTooltip: "Ce contenu n'est pas concerné par la modération.",
      moderClass: "disabled",
    };
  }

  if (hasPendingModeration) {
    return {
      moderTooltip:
        "Une mise-à-jour de ce modèle est en attente de modération.",
      moderClass: "orange",
    };
  }
  return {
    moderTooltip:
      "Aucune mise-à-jour de ce module est en attente de modération.",
    moderClass: "green",
  };
}
