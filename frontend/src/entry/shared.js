import { makeStyles } from "@material-ui/styles";
import green from "@material-ui/core/colors/green";
import amber from "@material-ui/core/colors/amber";

/**
 * Get the correct style for a color of notistack (not enough contrast some times with default settings)
 * @param color
 * @param theme
 */
export function getStyle(color, theme) {
  return {
    backgroundColor: color,
    color: theme.palette.getContrastText(color),
  };
}

export const useStylesNotiStack = makeStyles((theme) => ({
  variantSuccess: {
    ...getStyle(green[600], theme),
  },
  variantError: {
    ...getStyle(theme.palette.error.dark, theme),
  },
  variantInfo: {
    ...getStyle("#2979ff", theme),
  },
  variantWarning: {
    ...getStyle(amber[700], theme),
  },
}));
