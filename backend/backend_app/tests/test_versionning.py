from backend_app.tests.utils import WithUserTestCase
from backend_app.models.for_testing.versioning import ForTestingVersioning
from django.conf import settings
from reversion.models import Version
from backend_app.signals.squash_revisions import new_revision_saved
from django.test import override_settings
from django.contrib.contenttypes.models import ContentType


class VersioningTestCase(WithUserTestCase):
    @classmethod
    def setUpMoreTestData(cls):
        cls.testing_model = ForTestingVersioning
        cls.api_versioning = "/api/test/versioning/"

    def reset_signal_called(self):
        self.signal_was_called = False

    def test_setting_ok(self):
        self.assertTrue(settings.TESTING)

    #####

    def _submit_put_test(self, client, data, pk):
        response = client.put(self.api_versioning + str(pk) + "/", data, format="json")
        self.assertEqual(response.status_code, 200)
        return response

    def _test_retrieve_instance(self, data):
        matching = self.testing_model.objects.filter(bbb=data["bbb"])
        self.assertTrue(matching.exists())

        return matching[0]

    ####
    @override_settings(MODERATION_ACTIVATED=False)
    def test_versioning(self):
        """
        Test to check that versioning is working
        We also check that new_revision_saved is called
        We also check that the number of versions in the field is incremented
        """

        def _test_signal_sent(sender, obj, **kwargs):
            self.signal_was_called = True

        new_revision_saved.connect(_test_signal_sent)

        data_1 = {"bbb": "Test 1"}
        response = self.authenticated_client.post(
            self.api_versioning, data_1, format="json"
        )
        self.assertEqual(response.status_code, 201)
        instance = self._test_retrieve_instance(data_1)

        versions = Version.objects.get_for_object(instance)
        self.assertEqual(len(versions), 1)
        self.assertEqual(len(versions), instance.nb_versions)
        self.assertTrue(self.signal_was_called)
        self.reset_signal_called()

        data_2 = {"bbb": "Test 2"}
        response = self.authenticated_client_2.put(
            self.api_versioning + str(instance.pk) + "/", data_2, format="json"
        )
        self.assertEqual(response.status_code, 200)

        instance = self._test_retrieve_instance(data_2)
        versions = Version.objects.get_for_object(instance)
        self.assertEqual(len(versions), 2)
        self.assertEqual(len(versions), instance.nb_versions)
        self.assertTrue(self.signal_was_called)

        # Final test: we query the version viewset itself
        ct = ContentType.objects.get_for_model(instance).id
        self.authenticated_client.get("/api/versions/{}/{}/".format(ct, instance.pk))
