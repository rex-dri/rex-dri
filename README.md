# Rex-DRI

**This awesome app to share information regarding student exchanges is available at [assos.utc.fr/rex-dri/](https://assos.utc.fr/rex-dri/)** :tada:

_We hope you'll enjoy it !_ :wink:

## Documentation

The project documentation is available [here](https://rex-dri.gitlab.utc.fr/rex-dri/documentation/).

## Development

Development happens on the `dev` branch.

The current release in production can be found on the `master` branch.

---

**[Collaborators](./documentation/Other/contributions.md) and PR are highly welcomed!**

Feel free to ask questions about the project using the issue system.
