import PropTypes from "prop-types";
import React from "react";
import Grid from "@material-ui/core/Grid";

/**
 * Component to display several components on the same line with a Grid element
 * @param {object} props The components in props.children will be displayed on a same line
 * @constructor
 */
function SameLine(props) {
  return (
    <Grid
      container
      direction="row"
      justify="flex-start"
      alignItems="flex-start"
    >
      {props.children.map((child, key) => (
        // eslint-disable-next-line react/no-array-index-key
        <Grid item key={key}>
          {child}
        </Grid>
      ))}
    </Grid>
  );
}

SameLine.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
};

export default SameLine;
