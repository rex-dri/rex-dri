# Global State Handling

## Introduction

As you may know, react components have a strict _living_ period and as soon as they are _unmounted_ from the virtual DOM the data they contained in their **state** is completely deleted.

As a developer, there are often cases when you would like react components to share data between together or to persist the component's state. One of the way to accomplish that is by "lifting the state up" (in the component tree) which is not always practical nor optimal.

Another possibility is to use React's `Context` API (used in some places in REX-DRI) ; but it's hard to generalize / use.

?> But why not redux?

If you are familiar with Javascript frontend app development you may be familiar with `Redux` (and `react-redux`). Those libraries provide powerful global state management to your applications.

Still `react-redux` can be a bit hard to optimize; and `reducer` and `action` principles at his core can be a boilerplate nightmare for the developer (redux is really a "white elephant").

> **That's why, as of the version `2.0.0` of this project, we don't use redux anymore and now rely purely on customized react-hooks to persist and share states across components.**

## The useGlobalState hook

You will find in the project the `useGlobalState(key, initialValue)` hook.

```jsx
import useGlobalState from "...";

function MyComponent() {
  const [state, setState] = useGlobalState("my-counter", 0);

  return <button onClick={() => setState(state + 1)}>Counts: {state}</button>;
}
```

It behaves similarly as react's `useState` hook except:

- The state of that hook is persisted in a `globalState` (a Javascript variable)
  - So anything can access that state
- The state is identified by a `key`, so that the previous state is retrieved on component re-mount.
- This state is shared by any component using the `useGlobalState` hook with the same `key`
  - **In such a way that any update to that state in any component will update the state value in all other components that are using it.**

> **It's basically that simple.** As a developer you simply need to be careful about the `key` you use so that there is no overlapping possible.

## The useGlobalReducer hook

To mimic `redux` use of reducers, you can also use the `useGlobalReducer(key, reducer, initialState)` hook to perform update on the global / shared state of the app.

?> Under the hood `useGlobalReducer` uses `useGlobalState` so same notes applies here.

Here is basically the same example as previously with the `useGlobalReducer` hook.

```jsx
import useGlobalReducer from "...";

const ACTION_INCREMENT = "ACTION_INCREMENT";

const myReducer = (action, previousState) => {
  if (action === ACTION_INCREMENT) return previousState + 1;
  else return previousState;
};

function MyComponent() {
  const [state, dispatch] = useGlobalReducer("my-counter", myReducer, 0);

  return (
    <button onClick={() => dispatch(ACTION_INCREMENT)}>Counts: {state}</button>
  );
}
```

The idea is depending on the **action** you **dispatch** the associated **reducer** will compute the new **state** value based on the **previous state** value.

!> Reducers and `useGlobalReducer` are helpful when you have different and specific _actions_ that can be applied to a state. It is kind of a stricter and more explicit way of using the global state ; but it also more verbose.
