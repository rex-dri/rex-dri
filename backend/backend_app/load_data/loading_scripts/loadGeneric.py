from base_app.models import User
from django.utils import timezone

import reversion
from backend_app.models.abstract.essentialModule import EssentialModule


class LoadGeneric(object):
    """Class to handle the loading of initial data in a generic fashion
    """

    @classmethod
    def add_info_and_save(cls, obj: EssentialModule, admin: User):
        with reversion.create_revision():
            obj.moderated_by = admin
            obj.updated_by = admin
            obj.moderated_on = timezone.now()
            obj.updated_on = timezone.now()
            obj.save()
