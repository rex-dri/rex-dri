import { useTheme } from "@material-ui/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";

// Small hack to keep track of the last width there seem to be bug sometimes
let lastWidth = "xs";

export default function useWindowWidth() {
  const theme = useTheme();
  lastWidth =
    [...theme.breakpoints.keys].reverse().reduce((output, key) => {
      const matches = useMediaQuery(theme.breakpoints.only(key));
      return !output && matches ? key : output;
    }, null) || lastWidth;
  return lastWidth;
}
