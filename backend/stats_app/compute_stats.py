from datetime import datetime, timedelta
from collections import Counter
from typing import Optional

from backend_app.models.university import University


from stats_app.models import DailyConnections, DailyExchangeContributionsInfo
from stats_app.utils import (
    get_daily_connections,
    get_today_as_datetime,
    get_contributions_profiles,
)


def update_daily_connections(date: Optional[datetime] = None):
    if date is None:
        date = get_today_as_datetime() - timedelta(days=1)

    DailyConnections.objects.update_or_create(
        date=date, defaults=dict(nb_connections=get_daily_connections(date))
    )


def update_daily_exchange_contributions_info(date: Optional[datetime] = None):
    if date is None:
        date = get_today_as_datetime() - timedelta(days=1)

    contributions_profiles = get_contributions_profiles(date)

    nb_contributions_by_profile = Counter(contributions_profiles)
    for contribution_profile, nb_contributions in nb_contributions_by_profile.items():
        university = University.objects.get(pk=contribution_profile.university_pk)
        DailyExchangeContributionsInfo.objects.update_or_create(
            date=date,
            type=contribution_profile.type,
            major=contribution_profile.major,
            minor=contribution_profile.minor,
            exchange_semester=contribution_profile.exchange_semester,
            university=university,
            defaults=dict(nb_contributions=nb_contributions),
        )


def update_all_stats(date: Optional[datetime] = None):
    if date is None:
        date = get_today_as_datetime() - timedelta(days=1)

    update_daily_connections(date)
    update_daily_exchange_contributions_info(date)
