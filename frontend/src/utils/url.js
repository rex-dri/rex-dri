/**
 * Get the get param value in the url
 * @param key {string}
 */
export function getGetParamInUrl(key) {
  const getParamsInUrl = new URL(document.location).searchParams;
  return getParamsInUrl.get(key);
}

/**
 *
 * @param key {string} Key of the get parameter
 * @param value {string} Value of the get parameter
 * @param mode {"replace"|"push"|"reload"} replace: silent update of the history | push: add entry to history | reload: replace and hard reload the page
 */
export function setGetParamInUrl(key, value, mode = "replace") {
  const getParamsInUrl = new URL(document.location).searchParams;
  getParamsInUrl.set(key, value);

  if (mode === "replace") {
    window.history.replaceState(null, null, `?${getParamsInUrl.toString()}`);
  } else if (mode === "push") {
    window.history.pushState(null, null, `?${getParamsInUrl.toString()}`);
  } else if (mode === "reload") {
    setGetParamInUrl(key, value, "replace");
    window.location.reload();
  } else {
    // eslint-disable-next-line no-console
    console.error(`Unsupported mode '${mode}', doing nothing`);
  }
}
