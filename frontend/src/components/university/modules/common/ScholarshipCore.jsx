import PropTypes from "prop-types";
import React from "react";
import Scholarship from "./Scholarship";
import nullIfNaN from "../../../../utils/nullIfNaN";

export function getScholarshipDefaultModelData() {
  return {
    comment: "",
    frequency: null,
    amount_min: null,
    amount_max: null,
    other_advantages: "",
    title: "",
    short_description: "",
    importance_level: "-",
    currency: "EUR",
    obj_moderation_level: 0,
    useful_links: [],
  };
}

function ScholarshipCore({ rawModelData }) {
  const {
    comment,
    frequency,
    currency,
    short_description,
    amount_min,
    amount_max,
    other_advantages,
  } = rawModelData;

  return (
    <Scholarship
      currency={currency}
      frequency={frequency}
      shortDescription={short_description}
      amountMin={nullIfNaN(parseFloat(amount_min))}
      amountMax={nullIfNaN(parseFloat(amount_max))}
      otherAdvantages={other_advantages}
      comment={comment}
    />
  );
}

ScholarshipCore.propTypes = {
  rawModelData: PropTypes.shape({
    comment: PropTypes.string,
    frequency: PropTypes.string,
    currency: PropTypes.string,
    short_description: PropTypes.string,
    amount_min: PropTypes.string,
    amount_max: PropTypes.string,
    other_advantages: PropTypes.string,
  }).isRequired,
};

export default ScholarshipCore;
