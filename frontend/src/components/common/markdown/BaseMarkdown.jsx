/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
// Inspired by : https://github.com/mui-org/material-ui/blob/master/docs/src/pages/page-layout-examples/blog/Markdown.js

import React from "react";
import PropTypes from "prop-types";

import ReactMarkdown from "react-markdown";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import { darken, lighten } from "@material-ui/core/styles/colorManipulator";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/styles";
import TextLink, { getLinkColor } from "../TextLink";

// Custom styling for the rendered markdown
// To be used in hooks based components only
const useStyles = makeStyles((theme) => {
  const { palette } = theme;
  const backgroundTable =
    palette.type === "dark"
      ? lighten(palette.background.paper, 0.07)
      : darken(palette.background.paper, 0.02);
  const headerTable =
    palette.type === "dark"
      ? lighten(palette.background.paper, 0.13)
      : darken(palette.background.paper, 0.07);

  return {
    list: {
      color: palette.text.primary,
      marginBottom: theme.spacing(2),
    },
    code: {
      fontFamily: "monospace",
    },
    listItem: {
      marginTop: theme.spacing(0.75),
    },
    blockquote: {
      color: palette.text.secondary,
      borderLeftWidth: "5px",
      borderLeftStyle: "solid",
      borderLeftColor: getLinkColor(theme),
      borderRadius: "1px",
      margin: "1.5em 10px",
      paddingTop: "0px",
      paddingBottom: "0px",
      paddingRight: "10px",
      paddingLeft: "10px",
    },
    table: {
      // color: palette.text.primary,
    },
    tableHead: {
      fontSize: "14px",
      background: headerTable,
    },
    backgroundTable: {
      background: backgroundTable,
    },
    bold: {
      fontWeight: 700,
    },
    divider: {
      paddingBottom: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  };
});

// ///////////////////////////////////////
// / Renderers definition
// //////////////////////////////////////

const headingScale = ["h1", "h2", "h3", "h4", "h5", "h6", "subtitle1", "body2"];

function getHeadingRender(offset) {
  return ({ level, children }) => {
    let variant;
    const idx = level + offset - 1;

    if (idx < headingScale.length) {
      variant = headingScale[idx];
    } else {
      variant = headingScale[headingScale.length - 1];
    }

    return (
      <Typography gutterBottom variant={variant} style={{ fontWeight: 500 }}>
        {children}
      </Typography>
    );
  };
}

const ListRenderer = ({ ordered, children }) => {
  if (ordered) {
    return <ol className={useStyles().list}>{children}</ol>;
  }
  return <ul className={useStyles().list}>{children}</ul>;
};

const ListItemRenderer = ({ children }) => (
  <li className={useStyles().listItem}>
    <Typography component="span">{children}</Typography>
  </li>
);

const CodeRenderer = ({ value }) => (
  // className={classes.code} would be override by typography variant
  <Typography
    variant="body2"
    component="code"
    style={{ fontFamily: "monospace" }}
  >
    {value}
  </Typography>
);

const InlineCodeRenderer = ({ children }) => (
  <code className={useStyles().code}>{children}</code>
);

const BlockquoteRenderer = ({ children }) => (
  <blockquote className={useStyles().blockquote}>
    <em>{children}</em>
  </blockquote>
);

const TableRenderer = ({ children }) => (
  <Paper className={useStyles().backgroundTable}>
    <Table className={useStyles().table}>{children}</Table>
  </Paper>
);

const TableHeadRenderer = ({ children }) => (
  <TableHead
    classes={{ root: useStyles().bold }}
    className={useStyles().tableHead}
  >
    {children}
  </TableHead>
);

// // End of renderers definition

function getRenderers(headingOffset) {
  return {
    heading: getHeadingRender(headingOffset),
    list: ListRenderer,
    listItem: ListItemRenderer,
    paragraph: ({ children }) => <Typography paragraph>{children}</Typography>,
    link: (props) => <TextLink {...props} />,
    code: CodeRenderer,
    inlineCode: InlineCodeRenderer,
    blockquote: BlockquoteRenderer,
    table: TableRenderer,
    tableHead: TableHeadRenderer,
    tableBody: ({ children }) => <TableBody>{children}</TableBody>,
    tableRow: ({ children }) => <TableRow hover>{children}</TableRow>,
    tableCell: ({ children }) => <TableCell>{children}</TableCell>,
    thematicBreak: () => <Divider className={useStyles().divider} />,
  };
}

/**
 * Custom Markdown component renderer to make use of material UI
 */
function BaseMarkdown({ compileSource, source, headingOffset }) {
  const compiledSource = compileSource(source);
  const renderers = getRenderers(headingOffset);

  return (
    <div style={{ wordBreak: "break-word" }}>
      <ReactMarkdown
        renderers={renderers}
        allowedTypes={[
          ...Object.keys(renderers),
          "text",
          "emphasis",
          "root",
          "strong",
        ]} // Only allow custom nodes and basic ones
        mode="escape"
        source={compiledSource}
      />
    </div>
  );
}

BaseMarkdown.propTypes = {
  source: PropTypes.string.isRequired,
  compileSource: PropTypes.func,
  /**
   * Shift heading so that they are not too big
   */
  headingOffset: PropTypes.number,
};

BaseMarkdown.defaultProps = {
  headingOffset: 4,
  compileSource: (source) => source,
};

export default React.memo(BaseMarkdown);
