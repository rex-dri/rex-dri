import React from "react";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import LinkIcon from "@material-ui/icons/Link";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(2),
  },
  rootLinks: {
    display: "flex",
    justifyContent: "flex-start",
    flexWrap: "wrap",
  },
  chip: {
    margin: theme.spacing(1),
  },
}));

function UsefulLinks({ usefulLinks }) {
  const classes = useStyles();

  if (!Array.isArray(usefulLinks)) {
    return <></>;
  }

  const nbItems = usefulLinks.length;
  if (nbItems === 0) {
    return <></>;
  }

  const s = nbItems > 1 ? "s" : "";
  return (
    <div className={classes.container}>
      <Typography variant="caption">
        &nbsp; Source
        {s}
        &nbsp; :
      </Typography>
      <div className={classes.rootLinks}>
        {usefulLinks.map((el, idx) => (
          <Chip
            // eslint-disable-next-line react/no-array-index-key
            key={idx}
            icon={<LinkIcon />}
            label={el.description}
            className={classes.chip}
            color="secondary"
            onClick={() => open(el.url, "_blank")}
            variant="outlined"
          />
        ))}
      </div>
    </div>
  );
}

UsefulLinks.propTypes = {
  usefulLinks: PropTypes.array,
};

UsefulLinks.defaultProps = {
  usefulLinks: [],
};

export default React.memo(UsefulLinks);
