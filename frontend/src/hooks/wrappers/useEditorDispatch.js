import { useMemo } from "react";
import { useApi, useApiCreate, useApiUpdate } from "./api";

/**
 *
 * @param {string} name
 * @returns {{saveData,clearSaveError}}
 */
function useEditorDispatch(name) {
  let lastSave = "update";

  const [, dispatch, actions] = useApi(name, "one");
  const update = useApiUpdate(name);
  const create = useApiCreate(name);

  return useMemo(
    () => ({
      saveData: (data, onSuccessCallback = () => {}) => {
        if ("id" in data) {
          // it's an update
          lastSave = "update";
          update(data.id, data, onSuccessCallback);
        } else {
          // it's a create
          lastSave = "create";
          create(data, onSuccessCallback);
        }
      },
      clearSaveError: () => {
        // we need to clear the correct error
        // depending on if it was a save or a create
        switch (lastSave) {
          case "update":
            dispatch(actions.clearUpdateFailed());
            break;

          case "create":
            dispatch(actions.clearCreateFailed());
            break;

          default:
            throw new Error("Unsupported");
        }
      },
    }),
    []
  );
}

export default useEditorDispatch;
