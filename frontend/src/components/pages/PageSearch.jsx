import React from "react";
import Typography from "@material-ui/core/Typography";
import { compose } from "recompose";
import Search from "../search/Search";
import { withErrorBoundary } from "../common/ErrorBoundary";
import Filter from "../filter/Filter";
import { withPaddedPaper } from "./shared";
import UnlinkedPartners from "../app/UnlinkedPartners";

/**
 * Component corresponding to the page with the search university capabilities
 */
function PageSearch() {
  return (
    <>
      <Typography variant="h4" gutterBottom>
        Recherche d'une université
      </Typography>
      <UnlinkedPartners variant="summary" />
      <Filter />
      <Search />
    </>
  );
}

PageSearch.propTypes = {};

export default compose(withPaddedPaper(), withErrorBoundary())(PageSearch);
