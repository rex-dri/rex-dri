import React, { useMemo } from "react";

import { v4 as uuidv4 } from "uuid";
import BaseMap from "./BaseMap";

import "./map.scss";
import UniversityService from "../../services/data/UniversityService";
import CountryService from "../../services/data/CountryService";
import { useSelectedUniversities } from "../../hooks/wrappers/useSelectedUniversities";

const MAIN_MAP_ID = uuidv4();

/**
 * Main map of the application (map tab)
 */
function MainMap() {
  const [listUnivSel] = useSelectedUniversities();

  const univSelection = useMemo(() => {
    const out = [];
    const universities = UniversityService.getUniversities();

    universities.forEach((univ) => {
      if (univ) {
        out.push({
          univName: univ.name,
          univLogoUrl: univ.logo,
          cityName: univ.city,
          countryName: CountryService.getCountryForCountryId(univ.country).name,
          main_campus_lat: parseFloat(univ.main_campus_lat),
          main_campus_lon: parseFloat(univ.main_campus_lon),
          univId: univ.id,
          selected:
            listUnivSel === null ||
            (listUnivSel.length > 0 && listUnivSel.indexOf(univ.id) > -1),
        });
      }
    });

    return out;
  }, [listUnivSel]);

  // create all the markers
  return <BaseMap id={MAIN_MAP_ID} universities={univSelection} />;
}

MainMap.propTypes = {};

export default MainMap;
