import React from "react";
import PropTypes from "prop-types";
import { setDisplayName } from "recompose";
import AlertService from "../../services/AlertService";
import NavigationService from "../../services/NavigationService";
import { useApiCreate } from "../../hooks/wrappers/api";

function clear() {
  return {
    error: null,
    errorInfo: null,
  };
}

/**
 * Component to act as an error boundary, to prevent the app from deep unrecoverable crashes.
 */
class ErrorBoundary extends React.Component {
  state = clear();

  componentDidCatch(error, errorInfo) {
    // Catch errors in any components below and re-render with error message
    this.setState({
      error,
      errorInfo,
    });

    const data = "stack" in error ? { componentStack: error.stack } : errorInfo;

    this.props.logErrorOnServer(data);
  }

  render() {
    const { error, errorInfo } = this.state;
    // In case of error
    if (errorInfo) {
      // eslint-disable-next-line no-console
      console.log(error, errorInfo);
      AlertService.open({
        info: false,
        title:
          "Une erreur inconnue c'est produite dans l'application. Nous vous prions de nous en excuser.",
        description:
          "Nous vous invitons à recharger la page. Si l'erreur persiste, merci de contacter les administrateurs du site; l'erreur leur a été transmise.",
        agreeText: "C'est noté, je sais que vous faîtes de votre mieux :)",
        disagreeText: "Retourner à l'accueil",

        handleResponse: (agreed) => {
          this.setState(clear());
          // May need to click twice, but there seem to be no other ways
          if (!agreed) NavigationService.goHome();
        },
      });

      return <></>;
    }

    // Normally, just render children
    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired,
  logErrorOnServer: PropTypes.func.isRequired,
};

/**
 * HOC (higher order component) wrapper to provide an error boundary to the sub components.
 *
 * @returns {function(*): function(*): *}
 */
// eslint-disable-next-line import/prefer-default-export
export function withErrorBoundary() {
  return (Component) =>
    setDisplayName("error-boundary")(
      // We need to forward the ref otherwise the styles are not correctly applied.
      // eslint-disable-next-line react/display-name
      React.forwardRef((props, ref) => {
        const logErrorOnServer = useApiCreate("frontendErrors");

        return (
          <ErrorBoundary logErrorOnServer={logErrorOnServer}>
            <Component {...props} ref={ref} />
          </ErrorBoundary>
        );
      })
    );
}
