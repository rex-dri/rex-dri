#!/bin/bash

if [ ! -f `dirname $0`/db.env ]; then
    echo "db.env file doesn't exist, creating it with example/dev settings"
    cp `dirname $0`/db.ex.env `dirname $0`/db.env
else
    echo "db.env file already exists, not recreating it."
fi


if [ ! -f `dirname $0`/django.env ]; then
    echo "django.env file doesn't exist, creating it with example/dev settings"
    cp `dirname $0`/django.ex.env `dirname $0`/django.env
else
    echo "django.env file already exists, not recreating it."
fi


if [ ! -f `dirname $0`/external_data.env ]; then
    echo "external_data.env file doesn't exist, creating it with example/dev settings"
    cp `dirname $0`/external_data.ex.env `dirname $0`/external_data.env
else
    echo "external_data.env file already exists, not recreating it."
fi

if [ ! -f `dirname $0`/../../.env ]; then
    echo ".env file next to docker-compose doesn't exist, creating it with dev settings"
    cp `dirname $0`/ex.env `dirname $0`/../../.env
else
    echo ".env file already exists, not recreating it."
fi
