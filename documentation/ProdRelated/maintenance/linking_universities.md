# Linking Universities

## Problem description

Data from UTC DSI is automatically updated every day.
However, we need to link new university data from the DSI with universities on REX-DRI to guarantee data integrity (there might be duplicates on the DSI side) and correctness. Unlinked universities ids and names are displayed on this route: [https://rex.dri.utc.fr/app/about/unlinked-universities](https://rex.dri.utc.fr/app/about/unlinked-universities).

## Linking (DSI) partners and (REX-DRI) universities

On [https://rex.dri.utc.fr/admin/backend_app/partner/](https://rex.dri.utc.fr/admin/backend_app/partner/) select the Partner object with the correct id. Unlinked partners with their ids are visible at [https://rex.dri.utc.fr/app/about/unlinked-partners/](https://rex.dri.utc.fr/app/about/unlinked-partners/).

Then, there are two cases:

1. The university (on REX-DRI) already exists. Select the corresponding university in the dropdown list for the field "University".

2. The university does not exist. Create a new University object by clicking the + button for the field "University".
Fill all the requested information. 
- for "Updated by" and "Moderated by", select your own username
- for "Logo", select a random Logo in the dropdown list
- for "Denormalized infos", write "{}". It will be updated automatically.

Click on "ENREGISTRER"

## Updating denormalized information

After you have linked a partner to a university, you **must** update the denormalized information on the universities. Administrators can do it from the trigger_cron admin page: [https://rex.dri.utc.fr/admin/trigger_cron](https://rex.dri.utc.fr/admin/trigger_cron). Click on "Update extra denormalization" to update information about universities.
