from django.db.models.signals import post_save

from backend_app.models.country import Country
from backend_app.models.course import Course
from backend_app.models.courseFeedback import CourseFeedback
from backend_app.models.exchange import Exchange
from backend_app.models.exchangeFeedback import ExchangeFeedback
from backend_app.models.sharedUnivFeedback import SharedUnivFeedback
from backend_app.models.taggedItems import (
    UNIVERSITY_TAG_CHOICES,
    UniversityTaggedItem,
    CountryTaggedItem,
    COUNTRY_TAG_CHOICES,
)
from backend_app.models.university import University
from backend_app.models.universityInfo import UniversityInfo
from backend_app.models.universitySemestersDates import UniversitySemestersDates
from backend_app.models.userData import UserData
from backend_app.utils import get_module_defaults_for_bot, revision_bot
from base_app.models import User


def create_univ_modules(sender, instance: University, created, **kwargs):
    if created:
        defaults = get_module_defaults_for_bot()
        with revision_bot():
            UniversityInfo.objects.get_or_create(university=instance, defaults=defaults)
        with revision_bot():
            UniversitySemestersDates.objects.get_or_create(
                university=instance, defaults=defaults
            )
        with revision_bot():
            SharedUnivFeedback.objects.get_or_create(
                university=instance, defaults=defaults
            )


def create_user_modules(sender, instance, created, **kwargs):
    if created:
        UserData.objects.create(owner=instance)


def create_exchange_feedback_module(sender, instance: Exchange, created, **kwargs):
    if created:
        with revision_bot():
            ExchangeFeedback.objects.create(exchange=instance)


def create_course_feedback_module(sender, instance, created, **kwargs):
    if created:
        with revision_bot():
            CourseFeedback.objects.create(course=instance)


def create_tagged_items_university(sender, instance, created, **kwargs):
    if created:
        defaults = get_module_defaults_for_bot()
        for tag in UNIVERSITY_TAG_CHOICES:
            with revision_bot():
                UniversityTaggedItem.objects.get_or_create(
                    tag=tag, university=instance, defaults=defaults
                )


def create_tagged_items_country(sender, instance, created, **kwargs):
    if created:
        defaults = get_module_defaults_for_bot()
        for tag in COUNTRY_TAG_CHOICES:
            with revision_bot():
                CountryTaggedItem.objects.get_or_create(
                    tag=tag, country=instance, defaults=defaults
                )


def enable_auto_create():
    post_save.connect(create_univ_modules, sender=University)
    post_save.connect(create_user_modules, sender=User)
    post_save.connect(create_course_feedback_module, sender=Course)
    post_save.connect(create_exchange_feedback_module, sender=Exchange)
    post_save.connect(create_tagged_items_university, sender=University)
    post_save.connect(create_tagged_items_country, sender=Country)
