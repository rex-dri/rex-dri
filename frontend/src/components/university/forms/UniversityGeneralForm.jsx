import React from "react";

import TextField from "../../edition/fields/TextField";
import HiddenField from "../../edition/fields/HiddenField";
import FormInfo from "../../../utils/editionRelated/FormInfo";

function UniversityGeneralForm() {
  return (
    <>
      <TextField
        label={"Nom de l'université"}
        fieldMapping="name"
        required
        maxLength={200}
      />

      <TextField
        label={"Acronyme de l'université"}
        fieldMapping="acronym"
        maxLength={20}
      />

      <TextField
        label={"Site internet de l'université"}
        fieldMapping="website"
        maxLength={300}
        isUrl
      />
      {/*           Logo feature is not ready yet */}
      <HiddenField fieldMapping="logo" />
      {/* <TextField label={"Logo de l'université"} */}
      {/*           {...this.getReferenceAndValue("logo")} */}
      {/*           required={false} */}
      {/*           maxLength={300} */}
      {/*           isUrl={true} */}
      {/*           urlExtensions={["jpg", "png", "svg"]}/> */}
    </>
  );
}

export default new FormInfo("universities", UniversityGeneralForm);
