/* eslint-disable react/sort-comp */
import isEqual from "lodash/isEqual";
import CustomError from "../../components/common/CustomError";

/**
 * Function to build a cached version of the fields mapping to cascade update.
 *
 * @param {Array.<FormLevelError>} formLevelErrors
 * @returns {Map<string, Set.<string>>}
 */
function buildFieldUpdateCache(formLevelErrors) {
  const out = new Map();
  formLevelErrors
    .map((e) => e.getFields())
    .forEach((fieldList) =>
      fieldList.forEach((field) => {
        if (!out.has(field)) {
          out.set(field, new Set());
        }

        fieldList
          .filter((linkedField) => linkedField !== field)
          .forEach((linkedField) => {
            out.set(field, out.get(field).add(linkedField));
          });
      })
    );

  return out;
}

/**
 * FormManager
 *
 * It has a ton of custom behavior implemented in it. `Fields` and `Form`
 * are depending on each other with some custom subscribing behavior.
 */
class FormManager {
  /**
   * Set containing the field mappings
   * @type {Set<string>}
   */
  fieldsMappings = new Set();

  /**
   * Object containing a reference the setAllErrors function inside the useField hook
   * @type {object.<string,function(CustomError)>}
   */
  setErrorsByFieldMapping = {};

  /**
   * Array containing the possible form level errors
   * @type {Array.<FormLevelError>}
   */
  formLevelErrors = [];

  /**
   * Raw data of the model
   * @type {object}
   */
  modelData = {};

  /**
   * Updated combined data of the form
   * @type {object.<string,*>}
   */
  dataInFields = {};

  /**
   *
   * @type {object.<string, CustomError>}
   */
  errorsInFields = {};

  constructor(modelData, formLevelErrors = []) {
    this.modelData = { ...modelData };
    this.dataInFields = { ...modelData };
    this.formLevelErrors = formLevelErrors;
  }

  /**
   * @param {string} fieldMapping
   * @returns {*}
   */
  getInitialValueForMapping(fieldMapping) {
    return this.modelData[fieldMapping];
  }

  /**
   *
   * General Utils
   *
   */

  /**
   * Function to be used in fields so that they subscribe to a form
   *
   * Using ref to field is not working with withStyles ref forwarding issues.
   *
   * @param {string} fieldMapping
   * @param {CustomError} initialError
   * @param {function(CustomError)} setAllErrors - method used to set the error on the field
   */
  fieldSubscribe(fieldMapping, initialError, setAllErrors) {
    this.fieldsMappings.add(fieldMapping);
    this.errorsInFields[fieldMapping] = initialError;
    this.setErrorsByFieldMapping[fieldMapping] = setAllErrors;
  }

  /**
   * Returns the list of field mappings in the form
   * @returns {string[]}
   */
  getFieldsMapping() {
    return [...this.fieldsMappings];
  }

  /**
   * Function to look if there has been changes compared to the data
   * this is already saved.
   *
   * @returns {Boolean}
   */
  hasChanges() {
    const formData = this.getDataFromFields();
    const { modelData } = this;

    return Object.keys(formData).some((fieldMapping) => {
      const cmp1 = formData[fieldMapping];
      const cmp2 = modelData[fieldMapping];

      // we need to compare objects (ie JSON objects) differently
      if (typeof cmp1 === "object") {
        return !isEqual(cmp1, cmp2);
      }
      if (typeof cmp1 === "number" || typeof cmp2 === "number") {
        // eslint-disable-next-line eqeqeq
        return cmp1 != cmp2; // allow 93 == "93.00"
      }
      return cmp1 !== cmp2;
    });
  }

  /**
   *
   *
   *
   *
   * Error handling
   *
   *
   *
   */

  /**
   * Returns an object containing with {fieldMapping: valueInField}
   *
   * @returns {object.<string, any>}
   */
  getDataFromFields() {
    return this.dataInFields;
  }

  /**
   * Returns the error object corresponding to the errors **from the form** for a field.
   *
   * @param {string} fieldMapping
   * @returns {CustomError}
   */
  getErrorForFieldFromForm(fieldMapping) {
    const formData = this.getDataFromFields();
    const errorMessages = this.formLevelErrors
      .filter((formLevelError) =>
        formLevelError.getFields().includes(fieldMapping)
      )
      .filter((formLevelError) => formLevelError.hasError(formData))
      .map((formLevelError) => formLevelError.getMessage());
    return new CustomError(errorMessages);
  }

  /**
   * Function to call once a field has been updated, to trigger a rerendering of all the fields
   * linked by a form level error handling.
   * @param {string} fieldMapping
   * @param {*} value - new value of the field
   * @param {CustomError} fieldError - Error from the field
   */
  fieldUpdated(fieldMapping, value, fieldError) {
    this.dataInFields[fieldMapping] = value;
    this.errorsInFields[fieldMapping] = fieldError;

    // set error on this field
    this.updateFieldsErrors(fieldMapping);

    // Set errors on all other linked fields
    const fieldsToUpdate = this.getFieldsToUpdate();
    if (fieldsToUpdate.has(fieldMapping))
      this.updateFieldsErrors(...fieldsToUpdate.get(fieldMapping));
  }

  updateFieldsErrors(...fieldMappings) {
    fieldMappings.forEach((fieldMapping) => {
      const setError = this.setErrorsByFieldMapping[fieldMapping];
      const fieldError = this.errorsInFields[fieldMapping];
      const formErrorForField = this.getErrorForFieldFromForm(fieldMapping);
      setError(fieldError.combine(formErrorForField));
    });
  }

  /**
   * Helper function to cache the map containing the mapping between a field(name) and the set of fields
   * that should be updated when the previous one is updated.
   * @returns {ReactMapboxGl<string, Set>}
   */
  getFieldsToUpdate() {
    if (!this.fieldsToUpdate) {
      this.fieldsToUpdate = buildFieldUpdateCache(this.formLevelErrors);
    }
    return this.fieldsToUpdate;
  }

  /**
   * Function to know if the form has errors either at the field level
   * Or when running checks that combine fields
   *
   * @returns {CustomError}
   */
  getError() {
    const formData = this.getDataFromFields();

    const fieldsErrors = CustomError.superCombine(
      Object.values(this.errorsInFields)
    );

    const formErrors = new CustomError(
      this.formLevelErrors
        .filter((formLevelError) => formLevelError.hasError(formData))
        .map((formLevelError) => formLevelError.getMessage())
    );

    return fieldsErrors.combine(formErrors);
  }
}

export default FormManager;
