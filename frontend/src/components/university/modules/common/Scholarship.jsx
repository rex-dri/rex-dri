import React, { useCallback, useMemo } from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import Markdown from "../../../common/markdown/Markdown";
import CurrencyService from "../../../../services/data/CurrencyService";

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    alignItems: "center",
  },
  textAmount: {
    ...theme.typography.subtitle1,
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightMedium,
  },
  icon: {
    ...theme.typography.subtitle1,
    paddingRight: theme.spacing(0.5),
    paddingLeft: theme.spacing(0.5),
    color: theme.palette.text.primary,
  },
  frequency: {
    ...theme.typography.subtitle1,
    color: theme.palette.text.primary,
    paddingLeft: theme.spacing(1),
  },
  item: {
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightMedium,
  },
  spacer: {
    height: theme.spacing(2),
  },
}));

const FREQUENCIES = {
  w: "/semaine",
  m: "/mois",
  s: "/semestre",
  y: "/année",
  o: "(une seule fois)",
};

function Scholarship({
  currency,
  amountMin,
  amountMax,
  frequency,
  otherAdvantages,
  shortDescription,
  comment,
}) {
  const classes = useStyles();

  const frequencyStr = useMemo(() => FREQUENCIES[frequency], [frequency]);

  const convertAmountToEur = useCallback(
    (amount) => CurrencyService.convertAmountToEur(amount, currency),
    [currency]
  );

  const symbol = useMemo(() => CurrencyService.getCurrencySymbol(currency), [
    currency,
  ]);

  const amounts = useMemo(() => {
    if (amountMax != null && amountMax !== amountMin) {
      if (amountMin === null) {
        return `⩽ ${amountMax}${symbol}`;
      }
      return `${amountMin}${symbol} – ${amountMax}${symbol}`;
    }
    return `${amountMin}${symbol}`;
  }, [amountMin, amountMax, symbol]);

  const convertedAmounts = useMemo(() => {
    const convertedMin = convertAmountToEur(amountMin);

    // we only need for errors in conversion on min.
    // They share the same currency, so if one fails, the other will too.
    if (convertedMin === null) {
      return `Monnaie non reconnue: ${currency}`;
    }

    if (amountMax !== null && amountMax !== amountMin) {
      if (amountMin === null) {
        return `⩽ ${convertAmountToEur(amountMax)}€`;
      }
      return `${convertedMin}€ – ${convertAmountToEur(amountMax)}€`;
    }
    return `${convertedMin}€`;
  }, [currency, amountMin, amountMax, convertAmountToEur]);

  return (
    <>
      <Typography className={classes.item} variant="h5">
        {shortDescription}
      </Typography>
      <>
        {amountMin !== null || amountMax !== null ? (
          <>
            <Typography className={classes.item} variant="h5">
              Avantage financier :
            </Typography>
            <Typography className={classes.textAmount}>
              &nbsp;
              {amounts}
              &nbsp;
              {frequencyStr}
            </Typography>
            {currency !== "EUR" && (
              <Typography variant="caption">
                <em>
                  (≈
                  {convertedAmounts}
                  &nbsp;
                  {frequencyStr})
                </em>
              </Typography>
            )}
          </>
        ) : (
          <Typography variant="caption">
            <em>Aucun avantage financier est notifié.</em>
          </Typography>
        )}
      </>
      <div className={classes.spacer} />
      {otherAdvantages !== "" && otherAdvantages !== null && (
        <>
          <Typography className={classes.item} variant="h5">
            &nbsp; Autre(s) avantage(s) : &nbsp;
          </Typography>
          <Markdown source={otherAdvantages} />
        </>
      )}
      {comment && (
        <>
          <Typography className={classes.item} variant="h5">
            &nbsp; Informations complémentaires : &nbsp;
          </Typography>
          <Markdown source={comment} />
        </>
      )}
    </>
  );
}

Scholarship.propTypes = {
  currency: PropTypes.string,
  frequency: PropTypes.string,
  shortDescription: PropTypes.string.isRequired,
  comment: PropTypes.string.isRequired,
  otherAdvantages: PropTypes.string,
  amountMin: PropTypes.number,
  amountMax: PropTypes.number,
};

Scholarship.defaultProps = {
  currency: "",
  frequency: "",
  otherAdvantages: "",
  amountMin: null,
  amountMax: null,
};

export default React.memo(Scholarship);
