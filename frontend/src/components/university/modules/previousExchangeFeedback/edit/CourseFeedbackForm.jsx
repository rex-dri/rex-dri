import React from "react";
import MarkdownField from "../../../../edition/fields/MarkdownField";
import NumberField from "../../../../edition/fields/NumberField";
import { LanguageField } from "../../../../edition/fields/wrappedFields";
import FormInfo from "../../../../../utils/editionRelated/FormInfo";

const defaultNumberProps = {
  minValue: -5,
  maxValue: 5,
  required: true,
};

function CourseFeedbackForm() {
  return (
    <>
      <LanguageField />
      <NumberField
        label={"Votre appréciation de l'adéquation du cours dans votre cursus"}
        comment="Pas lié du tout (-5) < ça passe (0) < en plein dedans (5)"
        fieldMapping="adequation"
        {...defaultNumberProps}
      />

      <NumberField
        label={"Recommenderiez-vous ce cours d'une manière générale"}
        comment="Éviter (-5) < pourquoi pas (0) < foncer (5)"
        fieldMapping="would_recommend"
        {...defaultNumberProps}
      />

      <NumberField
        label="Votre appréciation de la facilité de suivre le cours (prérequis académiques et linguistiques)"
        comment="Très dur (-5) < ça passe (0) < très simple (5)"
        fieldMapping="following_ease"
        {...defaultNumberProps}
      />

      <NumberField
        label="Votre appréciation de la charge de travail du cours"
        comment="Très tranquile (-5) < parfait (0) < surchargé (5)"
        fieldMapping="working_dose"
        {...defaultNumberProps}
      />

      <MarkdownField
        label="Commentaire spécifique au cours"
        fieldMapping="comment"
        maxLength={1500}
        required
      />
    </>
  );
}

export default new FormInfo("courseFeedbacks", CourseFeedbackForm);
