from os.path import abspath, join

from backend_app.load_data.utils import ASSETS_PATH, csv_2_dict_list
from backend_app.models.country import Country
from base_app.models import User
from .loadGeneric import LoadGeneric


class LoadCountries(LoadGeneric):
    """
        Class to handle the loading of countries in the app
    """

    def __init__(self, admin: User):
        self.admin = admin

    @staticmethod
    def get_countries_data():
        country_file_loc = abspath(join(ASSETS_PATH, "country.csv"))
        return csv_2_dict_list(country_file_loc)

    @staticmethod
    def get_iso_conv():
        conv_alpha_file_loc = abspath(join(ASSETS_PATH, "alpha-conv-table.csv"))
        return csv_2_dict_list(conv_alpha_file_loc)

    def load(self):
        # Need to load the information for converting
        # countries alpha-3 code to alpha-2 code
        conv_alpha = {}
        for row in self.get_iso_conv():
            conv_alpha[row["alpha-3"]] = row["alpha-2"]

        for row in self.get_countries_data():
            code_iso_3 = str(row["ISO-alpha3 Code"])
            if code_iso_3 in conv_alpha.keys():
                code_alpha_2 = conv_alpha[code_iso_3]
            else:
                print(
                    "This country is not correctly identified and won't be inserted in db:",
                    row,
                )
                continue

            country = Country(
                name=row["Country or Area"],
                iso_alpha2_code=code_alpha_2,
                iso_alpha3_code=code_iso_3,
                region_name=row["Region Name"],
                region_un_code=row["Region Code"],
                sub_region_name=row["Sub-region Name"],
                sub_region_un_code=row["Sub-region Code"],
                intermediate_region_name=row["Intermediate Region Name"],
                intermediate_region_un_code=row["Intermediate Region Code"],
            )
            country.save()
            self.add_info_and_save(country, self.admin)
