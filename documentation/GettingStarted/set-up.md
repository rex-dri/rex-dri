# Set-up

## Git

_If you don't have `git` install on your computer, look online and install it :tongue: ._

Then, you can simply clone the project repository:

```bash
git clone git@gitlab.utc.fr:rex-dri/rex-dri.git && cd rex-dri
```

## Docker and docker-compose

This projects takes advantage of `docker` and `docker-compose` to ease the setup process, continuous integration, deployments, etc.

!> If you don't have those tools on your computer, you first need to install them. You can do so by following those guides:

- [`docker`](https://docs.docker.com/install/)
- [`docker-compose`](https://docs.docker.com/compose/install/)

_To run `Docker` your user needs to belong to the `docker` user-group (creating during the install process) or to `sudo` all `docker`-related commands. The last solution is a bit annoying when developing, so you can add your user to the `docker` group: `sudo usermod -aG docker your-user`. You can find "who" is your user with the command `whoami`. **The change will take effect after you restart your computer (or close your session and reopen it)**._

Once this is done, don't forget that you need to have docker running before using `docker-compose`. For example, on `Fedora 29` you need to run the following command:

```bash
sudo service docker start
```

Finally you can start-up all `docker` related _stuff_ using the command:

```bash
make dev
```

You can look at the `Makefile` to have more information on that last command and the other ones that are available.

To start all the `docker` images you will then only need to use the `make up` command.

**Important: the `make up` command actually lunches `docker-compose` in an _attached_ mode, so that you can easily access the logs of the docker containers. If you quit it (e.g. with `CTRL+C`), this will stop all the docker containers. So usually, you should lunch it in one shell, don't touch that shell and use another one for all other `make` command related to the containers that were lunched.**

## Initialization

To initialize the database associated with the project, you need to connect to the `backend` image:

_(As explained in the **important** section, this has to be done in another shell than the one you used for the `make up` command.)_

```bash
make shell_backend
```

Then:

- Migrate the `Django` models:

```bash
./manage.py migrate
```

- Create the initial revisions:

```bash
./manage.py createinitialrevisions
```

_NB: this last command should be run every time you migrate (modify/add) some models._

When this is done you can exit the `docker` image shell (`CTRL + D`).

## Checking setup

### Backend

Run the commands bellow to check that the backend is setup correctly:

_(Simple check of the setup)_.

```bash
make check_backend
```

_(Run `Python` tests)_

```bash
make test_backend
```

### Frontend

Run the commands bellow to check that the frontend is setup correctly:

_(Build the `React` app)_

```bash
make build_frontend
```

_NB: At the time of the writing of the documentation there is no test for the frontend..._

## You are done

Once the `Docker` images are up (command `make up`), the app is available on [http://localhost:8000](http://localhost:8000).

**Don't miss the other pages of the documentation, in particular [the one about loading 'init' data in the app](GettingStarted/init_data.md).**
