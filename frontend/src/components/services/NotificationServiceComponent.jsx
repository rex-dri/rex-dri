import React, { useEffect } from "react";
import { useSnackbar } from "notistack";
import NotificationService from "../../services/NotificationService";

/**
 * Component Used by the notification service to access the enqueueSnackbar method.
 */
function NotificationServiceComponent() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  useEffect(() => {
    NotificationService.setEnqueueSnackbar(enqueueSnackbar);
    NotificationService.setCloseSnackbar(closeSnackbar);
  }, [enqueueSnackbar, closeSnackbar]);

  return <></>;
}

export default React.memo(NotificationServiceComponent);
