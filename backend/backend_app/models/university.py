import logging

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

from backend_app.fields import JSONField
from backend_app.models.abstract.essentialModule import (
    EssentialModule,
    EssentialModuleSerializer,
    EssentialModuleViewSet,
)
from backend_app.models.file_picture import Picture
from backend_app.permissions.moderation import ModerationLevels
from backend_app.models.country import Country

logger = logging.getLogger("django")


class University(EssentialModule):
    """
    Model storing information about universities
    """

    moderation_level = ModerationLevels.ENFORCED

    name = models.CharField(max_length=200)
    acronym = models.CharField(max_length=20, default="", blank=True)
    logo = models.ForeignKey(Picture, null=True, on_delete=models.PROTECT)
    website = models.URLField(default="", blank=True, max_length=300)

    city = models.CharField(max_length=200)
    country = models.ForeignKey(Country, on_delete=models.PROTECT)

    main_campus_lat = models.DecimalField(
        max_digits=10,
        decimal_places=6,
        validators=[MinValueValidator(-85.05112878), MaxValueValidator(85.05112878)],
    )

    main_campus_lon = models.DecimalField(
        max_digits=10,
        decimal_places=6,
        validators=[MinValueValidator(-180), MaxValueValidator(180)],
    )

    # a bit of denormalization
    denormalized_infos = JSONField(default=dict)

    def location(self):
        return {"lat": self.main_campus_lat, "lon": self.main_campus_lon}


class UniversitySerializer(EssentialModuleSerializer):
    class Meta:
        model = University
        fields = EssentialModuleSerializer.Meta.fields + (
            "name",
            "acronym",
            "logo",
            "website",
            "city",
            "country",
            "main_campus_lat",
            "main_campus_lon",
            "denormalized_infos",
        )
        read_only_fields = (
            "city",
            "country",
            "main_campus_lat",
            "main_campus_lon",
            "denormalized_infos",
        )


class UniversityViewSet(EssentialModuleViewSet):
    permission_classes = EssentialModuleViewSet.permission_classes
    serializer_class = UniversitySerializer
    queryset = University.objects.all()  # pylint: disable=E1101
    end_point_route = "universities"


def update_denormalized_univ_field():
    logger.info("Computing the denormalized offers/semester/major in university")
    for university in University.objects.all().prefetch_related(
        "offers", "exchanges", "corresponding_utc_partners"
    ):
        semesters_majors_minors = dict()

        # handling of offers
        for offer in university.offers.all():
            semester = "{}{}".format(offer.semester, offer.year)
            if semester not in semesters_majors_minors.keys():
                semesters_majors_minors[semester] = dict()
            majors = offer.majors
            if majors is not None:
                possibilities = set(
                    map(lambda s: s.lstrip().rstrip(), majors.split(","))
                )
                for major in possibilities:
                    # No minors in offers
                    semesters_majors_minors[semester][major] = list()

        # handling of exchanges
        for exchange in university.exchanges.all():
            semester = "{}{}".format(exchange.semester, exchange.year)
            if semester not in semesters_majors_minors.keys():
                semesters_majors_minors[semester] = dict()
            major = exchange.student_major
            minor = exchange.student_minor
            if major is not None:
                if major not in semesters_majors_minors[semester].keys():
                    semesters_majors_minors[semester][major] = list()
                if (
                    minor is not None
                    and minor not in semesters_majors_minors[semester][major]
                ):
                    semesters_majors_minors[semester][major].append(minor)

        # handling of corresponding_utc_partners
        is_destination_open = (
            university.corresponding_utc_partners.filter(
                is_destination_open=True
            ).count()
            > 0
        )

        university.denormalized_infos = dict(
            is_destination_open=is_destination_open,
            semesters_majors_minors=semesters_majors_minors,
        )
        university.save()

    logger.info("Done the denormalized offers/semester/major in university")
