import React from "react";
import PropTypes from "prop-types";
import UnivContext from "../../contexts/UnivContext";
import UniversityService from "../../services/data/UniversityService";
import CountryService from "../../services/data/CountryService";

/**
 * Univ info provider to pre-fetch some data and make it available down the tree very easily
 */
function UnivInfoProvider({ univId, children }) {
  const univIdParsed = parseInt(univId, 10);
  const univ = UniversityService.getUniversityById(univIdParsed);
  const country = CountryService.getCountryForCountryId(univ.country);
  const { city } = univ;
  const countryId = country.id;

  return (
    <UnivContext.Provider
      value={{
        univId: univIdParsed,
        countryId,
        univ,
        city,
        country,
      }}
    >
      {children}
    </UnivContext.Provider>
  );
}

UnivInfoProvider.propTypes = {
  univId: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
};

export default UnivInfoProvider;
