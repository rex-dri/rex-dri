import InfoIcon from "@material-ui/icons/InfoOutlined";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { makeStyles } from "@material-ui/styles";
import { useSelectedUniversities } from "../../hooks/wrappers/useSelectedUniversities";

const useStyles = makeStyles((theme) => ({
  infoFilter: {
    marginLeft: theme.spacing(2),
    display: "inherit",
  },
}));

function getMessage(selectedUniversities) {
  const hasSelection = selectedUniversities !== null;

  if (!hasSelection) return "(Aucun filtre est actif)";
  const base = "(Un filtre est actif — ";

  const nbSelection = selectedUniversities.length;

  if (nbSelection === 0) return `${base}aucune université ne correspond)`;
  if (nbSelection === 1) return `${base}1 université correspond)`;
  return `${base}${nbSelection} universités correspondent)`;
}

function FilterStatus() {
  const classes = useStyles();

  const [selectedUniversities] = useSelectedUniversities();
  const hasSelection = selectedUniversities !== null;

  return (
    <div className={classes.infoFilter}>
      <InfoIcon color={hasSelection ? "primary" : "disabled"} />
      &nbsp;
      <Typography
        className={classes.caption}
        color={hasSelection ? "primary" : "textSecondary"}
      >
        <em>{getMessage(selectedUniversities)}</em>
      </Typography>
    </div>
  );
}

export default React.memo(FilterStatus);
