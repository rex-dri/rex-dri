import React from "react";
import PropTypes from "prop-types";
import keycode from "keycode";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/styles";
import useBlock from "../../../hooks/useBlock";
import DownshiftMultiple from "../../common/DownshiftMultiple";
import CustomNavLink from "../../common/CustomNavLink";
import APP_ROUTES from "../../../config/appRoutes";
import MetricFeedback from "../../common/MetricFeedback";
import OnBlurContainer from "../../common/OnBlurContainer";
import UniversityService from "../../../services/data/UniversityService";

const useStyle = makeStyles((theme) => ({
  paper: {
    ...theme.myPaper,
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

/**
 * Comment block that will hold markdown text
 */
function UnivBlock(props) {
  const [
    content,
    updateContent,
    renderAsInput,
    handleSwitchToInput,
    handleSwitchToDisplay,
  ] = useBlock(
    {
      university: props.university,
      appreciation: props.appreciation,
    },
    (v) => props.onChange(v),
    (v) => {
      // eslint-disable-next-line no-param-reassign
      if (v.university !== null) v.university = parseInt(v.university, 10);
      return v;
    },
    (v) => v.university === null,
    props.readOnly
  );

  const classes = useStyle();

  const univId = content.university;
  const { appreciation } = content;
  const univInfo = UniversityService.getUniversityById(univId);

  const options = UniversityService.getUniversitiesOptions();

  function handleAppreciationChange(value) {
    const regex = /[^\d]*/gi;
    let cleaned = parseInt(value.replace(regex, "").replace("-", ""), 10);
    if (isNaN(cleaned)) {
      cleaned = null;
    } else if (cleaned < 0) {
      cleaned = 0;
    } else if (cleaned > 10) {
      cleaned = 10;
    }
    updateContent((c) => ({
      ...c,
      appreciation: cleaned,
    }));
  }

  // link = APP_ROUTES.forUniversity(univ.id),
  // source = `# [${univ.name}](${link}) \n ${univ.city}, ${univ.country}`;

  const univPlaceHolder = "Entrez le nom de l'université à associer au block";
  return (
    <div
      role="presentation"
      onClick={handleSwitchToInput}
      onKeyDown={(e) => {
        if (e.ctrlKey && keycode(e) === "enter") handleSwitchToDisplay();
      }}
    >
      {renderAsInput ? (
        <OnBlurContainer onBlur={handleSwitchToDisplay}>
          <DownshiftMultiple
            multiple={false}
            options={options}
            fieldPlaceholder={univPlaceHolder}
            autoFocusInput
            onChange={(id) =>
              updateContent((c) => ({
                ...c,
                university: id,
              }))
            }
            value={univId !== null ? [univId] : undefined}
          />

          <div style={univId ? {} : { display: "none" }}>
            <Typography>Appréciation</Typography>
            <TextField
              placeholder={
                "Votre appréciation de l'établissement (entre 0 et 10)"
              }
              fullWidth
              multiline={false}
              value={appreciation === null ? "" : appreciation}
              type="number"
              inputProps={{
                min: 0,
                max: 10,
              }}
              required={false}
              onChange={(e) => handleAppreciationChange(e.target.value)}
            />
          </div>
        </OnBlurContainer>
      ) : (
        <>
          {univInfo && (
            <Paper className={classes.paper}>
              <div
                onClick={handleSwitchToInput}
                onFocus={handleSwitchToInput}
                role="presentation"
              >
                <Typography variant="h4">{univInfo.name}</Typography>

                {appreciation !== null && (
                  <MetricFeedback value={appreciation} min={0} max={10} />
                )}
              </div>
              <CustomNavLink to={APP_ROUTES.forUniversity(univId)}>
                <Button variant="outlined">
                  Se rendre sur la page de l'université
                </Button>
              </CustomNavLink>
            </Paper>
          )}
        </>
      )}
    </div>
  );
}

UnivBlock.propTypes = {
  onChange: PropTypes.func.isRequired,
  university: PropTypes.number,
  appreciation: PropTypes.number,
  readOnly: PropTypes.bool.isRequired,
};

UnivBlock.defaultProps = {
  university: null,
  appreciation: null,
};

UnivBlock.defaultProps = {};

export default React.memo(UnivBlock);
