Django==2.1.7
psycopg2-binary==2.7.7
django-cas-ng==3.6.0
djangorestframework==3.9.1
django-filter==2.1.0  # easy filtering on API
coreapi==2.3.3  # Automatic API doc generation
django-reversion==3.0.3
django-reversion-compare==0.8.6
pyyaml==3.13
uwsgi==2.0.18
dotmap==1.3.4
django-webpack-loader==0.6.0
python-dotenv==0.10.1
ipython==7.3.0  # For a better Django shell
jsonschema==3.0.1  # Validation of JSON data
rfc3987==1.3.8  # Validation of URI in JSON schema
