from re import compile

from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponse
from django.utils.deprecation import MiddlewareMixin
from rest_framework import permissions

EXEMPT_URLS = []
if hasattr(settings, "LOGIN_EXEMPT_URLS"):
    EXEMPT_URLS += [compile(str.lstrip("/")) for str in settings.LOGIN_EXEMPT_URLS]

AUTHORIZED_REQUEST_METHODS = list(permissions.SAFE_METHODS) + ["POST", "PUT", "DELETE"]


class RexDriRequestMiddleware(MiddlewareMixin):
    """
    This middleware performs different actions.

    - It checks that the HTTP request method is authorized on the plateform.


    - It requires a user to be authenticated to view any page other
    than LOGIN_URL. Exemptions to this requirement can optionally be specified
    in settings via a list of regular expressions in LOGIN_EXEMPT_URLS (which
    you can copy from your urls.py).

    Requires authentication middleware and template context processors to be
    loaded. You'll get an error if they aren't.
    """

    def process_request(self, request):
        # Check that the request.method is authorized on the site
        if request.method not in AUTHORIZED_REQUEST_METHODS:
            return HttpResponse("Unauthorized", status=401)

        assert hasattr(
            request, "user"
        ), "The RexDriRequestMiddleware\
 requires authentication middleware to be installed. Edit your\
 MIDDLEWARE_CLASSES setting to insert\
 'django.contrib.auth.middlware.AuthenticationMiddleware'. If that doesn't\
 work, ensure your TEMPLATE_CONTEXT_PROCESSORS setting includes\
 'django.core.context_processors.auth'."

        path = request.path_info.lstrip("/")
        full_path = request.get_full_path()

        # If the user is not authenticated redirect him/her to the login page
        if not request.user.is_authenticated:
            if not any(m.match(path) for m in EXEMPT_URLS):
                return HttpResponseRedirect(settings.LOGIN_URL + "?next=/" + path)

        else:
            # User is authenticated
            # We check if he / she has validated the CGU and the full RGPD
            if not request.user.has_validated_cgu_rgpd:
                if not path.startswith("cgu-rgpd"):
                    return HttpResponseRedirect("/cgu-rgpd/?next={}".format(full_path))
            # Handling of banned users
            elif request.user.is_banned and full_path != "/banned_note/":
                return HttpResponseRedirect("/banned_note/")
