import django.dispatch
from reversion.models import Version


def squash_revision_by_user(sender, obj, **kwargs):
    """
    It should also work with moderation as obj will be a versioned object
    """
    versions = (
        Version.objects.get_for_object(obj)
        .select_related("revision")
        .order_by("-revision__date_created")
    )

    last_edit = versions[0]
    user_made_last_edit = last_edit.revision.user

    if user_made_last_edit is not None:
        for v in versions[1:]:
            if v.revision.user == user_made_last_edit:
                v.delete()
            else:
                break

    # We update the number of versions directly here
    # So that it doesn't have to be recomputed every time
    obj.nb_versions = len(Version.objects.get_for_object(obj))
    obj.save()


new_revision_saved = django.dispatch.Signal(providing_args=["obj"])


def enable_squash_revisions():
    new_revision_saved.connect(
        squash_revision_by_user, dispatch_uid="receiver_concat_revisions"
    )
