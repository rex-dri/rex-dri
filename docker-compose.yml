# Main docker-compose file for the project to coordinate all the docker related stuff.
# To build from local image, use the `build: ...` attibute and not the `image` attribute.

# Main ports summary:
# - 8000: main app
# - 5000: documentation

version: "3.7"

volumes:
  # Create some local volume (should be stored in some directory on your computer)
  postgres_data:
  media_files:

networks:
  backend-nginx:
  backend-db:
  map-nginx:

services:
  # Service for the backend app.
  backend:
    # Get the image from the registry
    image: registry.gitlab.utc.fr/rex-dri/rex-dri/backend:v0.2.1
    # To use a locally built one, comment above, uncomment bellow.
    # build: ./backend
    restart: on-failure
    volumes: [".:/usr/src/app/", "media_files:/usr/src/app/backend/media"] # "Copy" the repo to the workdir and store media files on volume.
    networks: [backend-nginx, backend-db]
    environment:
      WAIT_HOSTS: database:5432 # For the 'wait' script, so that we are sure the db is up and running
    env_file:
      [
        ./server/envs/db.env,
        ./server/envs/django.env,
        ./server/envs/external_data.env,
      ]
    # Run the django developpement server on image startup.
    command: /bin/sh -c "/wait && cd backend && ./entry.sh && ./manage.py runserver 0.0.0.0:8000 --nostatic"
    # Required that the `database` and `frontend` service are up and running.
    depends_on: [database, frontend]

  # Serve through nginx just like in a production env
  nginx:
    build:
      context: ./server/nginx
      dockerfile: dev.Dockerfile
    restart: always
    volumes:
      - ./backend/static/:/usr/src/static:ro
      - media_files:/usr/src/media:ro
    networks: [backend-nginx, map-nginx]
    ports:
      # The port 8000 of the host is redirected to the port 80 of the container
      # which then redirect it to the port 8000 of the backend container
      - 8000:80
    depends_on: [backend, map]

  # Service for the postgres database
  database:
    # Use of classic image
    image: postgres:10.5-alpine
    networks: [backend-db]
    ports: ["5432:5432"]
    env_file: [./server/envs/db.env]
    volumes: ["postgres_data:/var/lib/postgresql/data/"] # Add a volume to store the DB data.

  # Service to handle frontend live developpments and building
  frontend:
    # Get the image from the registry
    image: registry.gitlab.utc.fr/rex-dri/rex-dri/frontend:v2.1.1
    # To use a locally built one, comment above, uncomment bellow.
    # build: ./frontend
    # On startup, we retrieve the dependencies from the image and start the developpement server
    command: /bin/sh -c "cd frontend && mv -f /usr/src/deps/node_modules/* /usr/src/deps/node_modules/.bin ./node_modules/ && yarn dev"
    volumes:
      # "Copy" the repo to the workdir.
      - .:/usr/src/app/
      # Ignore local node_modules: we will use the one from the docker image
      - /usr/src/app/frontend/node_modules
    ports:
      # Replicate the node server port (used for live/hot reloading). More info in ./frontend/server.js
      - 3000:3000
      # replicate the view stats port
      - "8888:8888"

  # Service to host map tiles
  map:
    image: floawfloaw/light-world-tileserver:2019-05-21--zoom-8
    networks: [map-nginx]
    volumes: ["./server/map:/data/custom:ro"]
    entrypoint:
      [
        "node",
        "/usr/src/app/",
        "-p",
        "8080",
        "--config",
        "/data/custom/config.json",
        "--public_url",
        "http://localhost:8000/map-server/",
        "--silent",
      ]
    restart: always

  # Service to provide a local documentation
  documentation:
    build: ./documentation
    volumes: ["./documentation:/usr/src/app"]
    # Start a simple python folder
    command: python run_server.py
    ports: ["5000:5000"] # replicate the server port

  # service to generate the UML of the backend
  gen_doc_uml:
    image: floawfloaw/plantuml
    volumes: [".:/usr/src/app"]
    command: tail -F anything # keep the container alive
