import React, { useCallback } from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";

import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import Markdown from "../../common/markdown/Markdown";
import Field from "./Field";

import TextLink from "../../common/TextLink";
import CustomError from "../../common/CustomError";
import useField from "../../../hooks/useField";
import FieldWrapper from "./FieldWrapper";

const defaultNullValue = "";

/**
 * Form field for markdown elements.
 * Props shouldn't be changed
 *
 * A `maxLength` can be set to limit the length of the text.
 */
function MarkdownField({ fieldMapping, required, label, comment, maxLength }) {
  const getError = useCallback((value) => {
    const messages = [];
    if (required && (value === "" || value === null)) {
      messages.push("Ce champ est requis mais il est vide.");
    }
    if (value !== null && maxLength && value.length > maxLength) {
      messages.push("Le texte est trop long.");
    }

    return new CustomError(messages);
  }, []);

  const [valueToDisplay, setValue, error] = useField(fieldMapping, {
    defaultNullValue,
    getError,
  });

  const handleChangeValue = useCallback((e) => {
    let { value } = e.target;

    if (maxLength && value.length > maxLength + 1) {
      value = value.substring(0, maxLength + 1);
    }

    setValue(value);
  }, []);

  return (
    <FieldWrapper
      required={required}
      errorObj={error}
      label={label}
      commentText={comment}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} md={12} lg={6}>
          <Typography variant="caption">
            <em>
              Saisie du texte (ce champ supporte en grande partie la syntaxe
              <TextLink href="https://www.markdownguide.org/basic-syntax">
                Markdown
              </TextLink>
              )
            </em>
          </Typography>
          {maxLength && (
            <Typography variant="caption" display="block">
              Nombre de caractères :{valueToDisplay.length}/{maxLength}
            </Typography>
          )}
          <TextField
            placeholder="Le champ est vide"
            fullWidth
            multiline
            value={valueToDisplay}
            onChange={handleChangeValue}
          />
        </Grid>

        <Grid item xs={12} md={12} lg={6}>
          <Typography variant="caption">
            <em>Prévisualisation du texte</em>
          </Typography>
          {typeof value === "string" && valueToDisplay.length === 0 ? (
            <Typography variant="caption" display="block">
              Le champ est vide
            </Typography>
          ) : (
            <Markdown source={valueToDisplay} />
          )}
        </Grid>
      </Grid>
    </FieldWrapper>
  );
}

MarkdownField.defaultProps = {
  ...Field.defaultProps,
  maxLength: 0,
};

MarkdownField.propTypes = {
  ...Field.propTypes,
  maxLength: PropTypes.number,
};

export default MarkdownField;
