import React, { useCallback } from "react";
import { compose } from "recompose";
import Typography from "@material-ui/core/Typography";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import SunnyIcon from "@material-ui/icons/WbSunny";
import WinterIcon from "@material-ui/icons/AcUnit";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import APP_ROUTES from "../../config/appRoutes";
import Loading from "../common/Loading";
import { withErrorBoundary } from "../common/ErrorBoundary";
import { withPaddedPaper } from "./shared";
import { CURRENT_USER_ID } from "../../config/user";
import RequestParams from "../../utils/api/RequestParams";
import compareSemesters from "../../utils/compareSemesters";
import TextLink from "../common/TextLink";
import UniversityService from "../../services/data/UniversityService";
import NavigationService from "../../services/NavigationService";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";
import {
  useApi,
  useApiInvalidateAll,
  useApiRead,
} from "../../hooks/wrappers/api";

function Introduction() {
  const invalidateData = useApiInvalidateAll("exchanges");
  const [, read] = useApiRead("updateStudentExchanges", "all");

  const requestReload = useCallback(() => {
    read(
      RequestParams.Builder.withOnSuccessCallback(() =>
        invalidateData()
      ).build()
    );
  }, []);

  return (
    <>
      <div>
        <Typography>
          Vous avez effectué un échange qui n'est pas listé ci-dessous ?
          Vérifier que vous avez&nbsp;
          <TextLink href="https://webapplis.utc.fr/etudiant/suiviOutgoing/autorisationsREX.faces">
            donné les autorisations de partage d'informations (
            <b>cours et login</b>) avec la plateforme REX-DRI depuis l'ENT.
          </TextLink>
          &nbsp;Puis, cliquez sur le bouton ci-dessous.
        </Typography>
      </div>
      <div
        style={{
          margin: "0 auto",
          width: "fit-content",
        }}
      >
        <Button variant="outlined" color="secondary" onClick={requestReload}>
          J'ai donné toutes les autorisations, recharger mes échanges depuis
          l'ENT
        </Button>
      </div>
    </>
  );
}

/**
 * Page that lists the previous exchange of a user
 */
function PageMyExchanges({ exchanges }) {
  const [{ isReading: isReloadingFromEnt }] = useApi(
    "updateStudentExchanges",
    "all"
  );

  exchanges.sort(
    (a, b) => -compareSemesters(a.year, a.semester, b.year, b.semester)
  );

  if (isReloadingFromEnt) {
    return (
      <>
        <Typography variant="h4">Rechargement en cours</Typography>
        <Loading />
      </>
    );
  }
  if (exchanges.length === 0) {
    return (
      <>
        <Introduction />
        <Typography variant="h4">
          Aucun échange n'est associé à votre compte.
        </Typography>
      </>
    );
  }
  return (
    <>
      <Introduction />
      <Typography variant="h4">Les échanges que j'ai effectué</Typography>
      <Typography variant="caption">
        (cliquer sur un départ pour éditer votre feedback sur ce dernier)
      </Typography>
      <List component="nav" aria-label="Liste des échanges réalisés">
        {exchanges.map((el) => (
          <ListItem
            button
            key={el.id}
            onClick={() =>
              NavigationService.goToRoute(
                APP_ROUTES.editForPreviousExchange(el.id)
              )
            }
          >
            <ListItemIcon>
              {el.semester === "a" ? <WinterIcon /> : <SunnyIcon />}
            </ListItemIcon>
            <ListItemText
              primary={`${el.semester.toUpperCase()}${
                el.year
              } | ${UniversityService.getUnivName(el.university)}`}
            />
          </ListItem>
        ))}
      </List>
    </>
  );
}

PageMyExchanges.propTypes = {
  exchanges: PropTypes.array.isRequired,
};

export default compose(
  withNetworkWrapper([
    new NetWrapParam("exchanges", "all", {
      prop: "exchanges",
      params: RequestParams.Builder.withQueryParam(
        "student",
        CURRENT_USER_ID
      ).build(),
    }),
  ]),
  withPaddedPaper(),
  withErrorBoundary()
)(PageMyExchanges);
