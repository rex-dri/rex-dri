# Update majors / minors

In case of changes of majors / minors at the UTC, the frontend filter utils need to be updated.

In `frontend/src/utils/majorMinorMappings.js`, each change has to be added as a new line in either `majorMapping` or `minorMapping` like this:

```js
export const majorMapping = {
  oldMajorName: "newMajorName",
};

export const minorMapping = {
  majorName: {
    oldMinorName: "newMinorName",
  },
};
```
