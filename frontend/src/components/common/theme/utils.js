/**
 * We put some stuff in a separate file to prevent useless imports for CGU
 */

import {
  createMuiTheme,
  responsiveFontSizes,
  rgbToHex,
} from "@material-ui/core/styles";
import "typeface-roboto";

const siteSettings = {
  typography: {
    fontSize: 13,
    htmlFontSize: 13,
  },
  myPaper: {
    padding: 16,
  },
};

/**
 * method to set the correct meta tags on the HTML page so that
 * the status bar on phone is of a matching color as the theme.
 *
 * @param mainColor
 */
export function updatePhoneStatusBarColor(mainColor) {
  const color = rgbToHex(mainColor);

  try {
    // eslint-disable-next-line no-restricted-syntax
    for (const el of document.getElementsByTagName("meta")) {
      if (
        el.name === "theme-color" ||
        el.name === "apple-mobile-web-app-status-bar-style"
      ) {
        el.content = color;
      }
    }
  } catch (e) {
    // nothing, yes.
  }
}

/**
 * Method to generate a full site theme based on an object themeData
 * @param themeData Should be an object like: src/config/defaultTheme.json
 * @returns {Theme}
 */
export function getTheme(themeData) {
  const type = themeData.mode;
  const palette = {
    type,
    primary: { main: themeData[type].primary },
    secondary: { main: themeData[type].secondary },
  };

  const themeObj = {
    ...siteSettings,
    palette,
  };
  const theme = createMuiTheme(themeObj);
  return responsiveFontSizes(theme);
}
