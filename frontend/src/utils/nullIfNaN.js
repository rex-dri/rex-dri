/**
 * If the value is NaN then it will be returned as null.
 *
 * @param {*} value
 * @returns {null|*}
 */
export default function nullIfNaN(value) {
  if (isNaN(value)) return null;
  return value;
}
