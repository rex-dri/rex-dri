import { useState } from "react";

function useStepper(initial = 0) {
  const [current, setCurrent] = useState(initial);

  const goNext = () => setCurrent(current + 1);
  const goPrevious = () => setCurrent(current - 1);
  const goTo = (idx) => setCurrent(idx);
  return [current, goNext, goPrevious, goTo];
}

export default useStepper;
