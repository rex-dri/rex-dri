module.exports = {
  env: {
    browser: true,
    es6: true,
    "jest/globals": true,
  },
  extends: ["airbnb", "prettier"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: "module",
  },
  plugins: ["react", "jest", "import"],
  rules: {
    "no-warning-comments": [
      "warn",
      {
        terms: ["todo", "fixme"],
        location: "anywhere",
      },
    ],
    "no-console": "error",
    // Styling choices bellow...
    "react/destructuring-assignment": "off", // might be less readable (and is a pain in the ass to refactor)
    "react/no-unescaped-entities": "off", // that one doesn't improve code readability
    "react/forbid-prop-types": "off",
    "react/jsx-wrap-multilines": "off",
    "react/jsx-props-no-spreading": "off",
    "react/state-in-constructor": "off",
    "react/jsx-one-expression-per-line": "off",
    "react/jsx-curly-newline": "off",
    "jsx-a11y/accessible-emoji": "off",
    "no-underscore-dangle": "off",
    "class-methods-use-this": "off",
    "arrow-parens": "off",
    "no-restricted-globals": "off",
    "one-var": "off",
    camelcase: "off", // hard to do with python convention
    "max-classes-per-file": "off",
    "max-len": "off",
  },
};
