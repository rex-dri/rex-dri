import React, { useCallback, useEffect } from "react";
import PropTypes from "prop-types";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Pagination from "@material-ui/lab/Pagination";
import { makeStyles } from "@material-ui/styles";
import range from "../../utils/range";
import APP_ROUTES from "../../config/appRoutes";
import CustomNavLink from "../common/CustomNavLink";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 650,
    marginLeft: "auto",
    marginRight: "auto",
    flexGrow: 1,
  },
  header: {
    display: "flex",
    alignItems: "center",
    height: 50,
    paddingLeft: theme.spacing(4),
    marginBottom: 20,
    backgroundColor: theme.palette.background.default,
  },
  center: {
    margin: "0 auto",
    width: "max-content",
  },
}));

/**
 * Component to provide a nice list of universities that can be swiped
 */
function UnivList({ universitiesToList, itemsPerPage }) {
  const classes = useStyles();
  const [page, setPage] = React.useState(1);

  useEffect(() => {
    // If the list is not the same, then make sure to "reset" and move to the first page
    setPage(1);
  }, [universitiesToList]);

  const numberOfItems = universitiesToList.length;
  const maxSteps = Math.ceil(numberOfItems / itemsPerPage);

  const getIndexesOnPage = useCallback(
    (currentPage) => {
      const firstIdxOnPage = currentPage * itemsPerPage;
      const lastIdxOnPage = Math.min(
        (currentPage + 1) * itemsPerPage,
        numberOfItems
      );
      return range(lastIdxOnPage - firstIdxOnPage).map(
        (el) => el + firstIdxOnPage
      );
    },
    [itemsPerPage, numberOfItems]
  );

  // Prevent bug
  if (!numberOfItems) {
    return <></>;
  }

  const handleChange = (event, value) => {
    setPage(value);
  };

  return (
    <div className={classes.root}>
      <List component="nav">
        <Divider />
        {getIndexesOnPage(page - 1).map((univIdx) => (
          <CustomNavLink
            to={APP_ROUTES.forUniversity(universitiesToList[univIdx].id)}
            key={univIdx}
          >
            <ListItem button divider key={univIdx}>
              <ListItemText primary={universitiesToList[univIdx].name} />
            </ListItem>
          </CustomNavLink>
        ))}
      </List>
      <div className={classes.center}>
        <Pagination
          count={maxSteps}
          page={page}
          onChange={handleChange}
          showFirstButton
          showLastButton
          color="secondary"
        />
      </div>
    </div>
  );
}

UnivList.propTypes = {
  universitiesToList: PropTypes.array.isRequired,
  itemsPerPage: PropTypes.number,
};

UnivList.defaultProps = {
  itemsPerPage: 10,
};

export default UnivList;
