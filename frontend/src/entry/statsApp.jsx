import React from "react";
import ReactDOM from "react-dom";
import { SnackbarProvider } from "notistack";
import CssBaseline from "@material-ui/core/CssBaseline";
import OfflineThemeProvider from "../components/common/theme/OfflineThemeProvider";
import Logo from "../components/app/Logo";
import BaseTemplate from "../components/app/BaseTemplate";
import PageStats from "../components/pages/PageStats";
import SnackbarCloseButton from "../components/app/SnackbarCloseButton";
import { useStylesNotiStack } from "./shared";
import NotificationServiceComponent from "../components/services/NotificationServiceComponent";

function SubEntry() {
  const classesNotistack = useStylesNotiStack();

  const toolbarContent = (
    <div
      style={{
        margin: "0 auto",
      }}
    >
      {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
      <div
        style={{ width: "fit-content" }}
        onClick={() => {
          window.history.pushState(null, null, "/app");
          window.location.reload();
        }}
      >
        <Logo />
      </div>
    </div>
  );

  return (
    <SnackbarProvider
      maxSnack={3}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      action={(key) => <SnackbarCloseButton notiKey={key} />}
      classes={{ ...classesNotistack }}
    >
      <>
        <CssBaseline />
        <main>
          <BaseTemplate toolbarContent={toolbarContent}>
            <NotificationServiceComponent />
            <PageStats />
          </BaseTemplate>
        </main>
      </>
    </SnackbarProvider>
  );
}

function MainReactEntry() {
  return (
    <OfflineThemeProvider>
      <SubEntry />
    </OfflineThemeProvider>
  );
}

const wrapper = document.getElementById("app");
ReactDOM.render(<MainReactEntry />, wrapper);

// eslint-disable-next-line no-undef
if (module.hot) {
  // eslint-disable-next-line no-undef
  module.hot.accept();
}
