import React from "react";
import PropTypes from "prop-types";
import alasql from "alasql";
import Button from "@material-ui/core/Button";
import DownloadIcon from "@material-ui/icons/CloudDownload";

/**
 * Component to have a clickable button to export to CSV
 */
function ExportToCsvButton({ data, label }) {
  return (
    <Button
      variant="outlined"
      color="secondary"
      onClick={() => {
        alasql(
          'SELECT * INTO CSV("export_rex_dri.csv", {headers:true}) FROM ?',
          [data]
        );
      }}
    >
      <DownloadIcon />
      &nbsp;{label}
    </Button>
  );
}

ExportToCsvButton.propTypes = {
  data: PropTypes.array.isRequired,
  label: PropTypes.string.isRequired,
};

export default ExportToCsvButton;
