import reversion

from backend_app.load_data.loading_scripts.loadBaseUsers import LoadBaseUsers
from backend_app.load_data.loading_scripts.loadCountries import LoadCountries
from backend_app.load_data.loading_scripts.loadCurrencies import LoadCurrencies
from backend_app.load_data.loading_scripts.loadGroups import LoadGroups
from backend_app.load_data.loading_scripts.loadLanguages import LoadLanguages
from backend_app.load_data.loading_scripts.loadRecommendationLists import (
    LoadRecommendationLists,
)
from backend_app.load_data.loading_scripts.loadSiteInformation import (
    LoadSiteInformation,
)
from backend_app.load_data.loading_scripts.loadUniversities import LoadUniversities
from backend_app.load_data.loading_scripts.loadUniversityEx import LoadUniversityEx


def load_all():
    """
    Function to load all the initial data in the app
    """

    with reversion.create_revision():
        LoadGroups()
        admin = LoadBaseUsers().get_admin()
        LoadCurrencies(admin).load()
        LoadCountries(admin).load()
        LoadUniversities(admin).load()
        LoadLanguages().load()
        LoadUniversityEx(admin).load()
        LoadRecommendationLists(admin).load()
        LoadSiteInformation(admin).load()


def load_prod():
    """
    for PRODUCTION ENV
    Function to load all the initial data in the app
    """

    with reversion.create_revision():
        LoadGroups()
        admin = LoadBaseUsers().get_admin()
        LoadCurrencies(admin).load()
        LoadCountries(admin).load()
        LoadLanguages().load()
        LoadSiteInformation(admin).load()
        # LoadUniversitiesProd().load()
        # LoadUniversityEx(admin).load()
        # LoadRecommendationLists(admin).load()
