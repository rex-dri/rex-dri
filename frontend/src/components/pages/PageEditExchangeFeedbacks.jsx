import React from "react";
import { compose } from "recompose";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import { withErrorBoundary } from "../common/ErrorBoundary";
import { withPaddedPaper } from "./shared";
import EditModuleGeneralPreviousExchangeFeedback from "../university/modules/previousExchangeFeedback/edit/EditModuleGeneralFeedback";
import RequestParams from "../../utils/api/RequestParams";
import EditModuleCoursesFeedback from "../university/modules/previousExchangeFeedback/edit/EditModuleCoursesFeedback";
import CustomLink from "../common/CustomLink";
import APP_ROUTES from "../../config/appRoutes";
import UniversityService from "../../services/data/UniversityService";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";

function getExchangeId(match) {
  return parseInt(match.params.exchangeId, 10);
}

const useStyles = makeStyles((theme) => ({
  spacer: {
    marginBottom: theme.spacing(4),
  },
  container: {
    maxWidth: 1200,
    margin: "0 auto",
  },
}));

/**
 * Page that handle the modification of exchange feedbacks.
 */
function PageEditExchangeFeedbacks({ match, exchange }) {
  const classes = useStyles();
  const id = getExchangeId(match);

  const univId = exchange.university;
  const univName = UniversityService.getUnivName(univId);

  if (id) {
    return (
      <>
        <Typography variant="h4">
          {univId ? (
            <CustomLink to={APP_ROUTES.forUniversity(univId)}>
              {univName}
            </CustomLink>
          ) : (
            univName
          )}
          &nbsp; ({exchange.semester.toUpperCase()}
          {exchange.year})
        </Typography>
        <Typography variant="h5">
          Page d'édition du feedback concernant votre échange.
        </Typography>
        <Typography>
          <em>
            Les informations issues de l'ENT (ECTS, etc.) sont modifiables
            uniquement depuis l'ENT.
          </em>
        </Typography>

        <div className={classes.spacer} />
        <div className={classes.container}>
          <EditModuleGeneralPreviousExchangeFeedback exchangeId={id} />
          <div className={classes.spacer} />
          <EditModuleCoursesFeedback exchangeId={id} />
        </div>
      </>
    );
  }
  return <></>;
}

PageEditExchangeFeedbacks.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      exchangeId: PropTypes.string.isRequired,
    }),
  }).isRequired,
  exchange: PropTypes.object.isRequired,
};

const buildParams = (match) =>
  RequestParams.Builder.withId(getExchangeId(match)).build();

export default compose(
  withNetworkWrapper([
    new NetWrapParam("exchanges", "one", {
      addDataToProp: "exchange",
      params: (props) => buildParams(props.match),
      propTypes: {
        match: PropTypes.shape({
          params: PropTypes.shape({
            exchangeId: PropTypes.string.isRequired,
          }),
        }).isRequired,
      },
    }),
  ]),
  withPaddedPaper(),
  withErrorBoundary()
)(PageEditExchangeFeedbacks);
