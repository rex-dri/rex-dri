import React, { useCallback, useEffect, useState } from "react";
import Alert from "../common/Alert";
import AlertService from "../../services/AlertService";

/**
 * Component to display an alert.
 */
function AlertServiceComponent() {
  const [alertProps, setAlertProps] = useState({ open: false });

  useEffect(() => {
    AlertService.setSetAlertProps(setAlertProps);

    return () => AlertService.setSetAlertProps(() => {});
  }, []);

  const handleClose = useCallback(() => setAlertProps({ open: false }), []);

  return <Alert {...alertProps} handleClose={handleClose} />;
}

export default React.memo(AlertServiceComponent);
