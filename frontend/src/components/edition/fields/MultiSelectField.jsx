import React, { useCallback, useMemo } from "react";
import PropTypes from "prop-types";

import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import Select from "@material-ui/core/Select";
import Checkbox from "@material-ui/core/Checkbox";

import Field from "./Field";
import CustomError from "../../common/CustomError";
import DownshiftMultiple from "../../common/DownshiftMultiple";
import useField from "../../../hooks/useField";
import FieldWrapper from "./FieldWrapper";

/**
 * Form field for multi selects.
 */
function MultiSelectField({ fieldMapping, required, label, comment, options }) {
  const optionsByValue = useMemo(
    () => new Map(options.map((opt) => [opt.value, opt.label])),
    []
  );

  const getError = useCallback((v) => {
    const messages = [];
    if (required && v.length === 0) {
      messages.push("Ce champ est requis.");
    }
    return new CustomError(messages);
  }, []);

  const [value, setValueInt, error] = useField(fieldMapping, { getError });
  const setValue = useCallback((e) => setValueInt(e.target.value), []);

  const renderSelected = useCallback(
    (selected) => selected.map((el) => optionsByValue.get(el)).join(", "),
    []
  );

  // for performance considerations
  const RenderComp = () =>
    options.length < 10 ? (
      <Select
        value={value}
        onChange={setValue}
        multiple
        renderValue={renderSelected}
      >
        {options.map((el) => (
          <MenuItem disabled={el.disabled} key={el.value} value={el.value}>
            <Checkbox checked={value.indexOf(el.value) > -1} />
            <ListItemText primary={el.label} />
          </MenuItem>
        ))}
      </Select>
    ) : (
      <DownshiftMultiple
        multiple
        value={value}
        options={options}
        fieldPlaceholder={label}
        onChange={setValueInt}
      />
    );

  return (
    <FieldWrapper
      required={required}
      errorObj={error}
      label={label}
      commentText={comment}
    >
      <RenderComp />
    </FieldWrapper>
  );
}

MultiSelectField.defaultProps = {
  ...Field.defaultProps,
};

MultiSelectField.propTypes = {
  ...Field.propTypes,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([
        PropTypes.number.isRequired,
        PropTypes.string.isRequired,
      ]),
      disabled: PropTypes.bool,
    })
  ),
};

export default MultiSelectField;
