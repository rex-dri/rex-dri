/**
 * Function to extract date time information from a string of format
 * "yyyy-mm-dd hh:mm:ss"
 *
 * @export
 * @param {string} dateTime
 * @returns {object}
 */
export default function dateTimeStrToStr(dateTime) {
  const reg = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
  const res = reg.exec(dateTime);
  const yyyy = res[1];
  const mm = res[2];
  const dd = res[3];
  const hh = res[4];
  const min = res[5];
  const sec = res[6];

  return {
    date: `${dd}/${mm}/${yyyy}`,
    time: `${hh}h${min}`,
    fullTime: `${hh}h${min}min${sec}s`,
  };
}
