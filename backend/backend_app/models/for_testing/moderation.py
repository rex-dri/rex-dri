from django.db import models

from backend_app.models.abstract.essentialModule import (
    EssentialModule,
    EssentialModuleSerializer,
    EssentialModuleViewSet,
)
from backend_app.permissions.moderation import ModerationLevels


class ForTestingModeration(EssentialModule):
    """
        Simple model for testing purposes
    """

    moderation_level = ModerationLevels.DEPENDING_ON_SITE_SETTINGS

    aaa = models.CharField(max_length=100)


class ForTestingModerationSerializer(EssentialModuleSerializer):
    """
        Simple serializer for testing purpose
    """

    class Meta:
        model = ForTestingModeration
        fields = EssentialModuleSerializer.Meta.fields + ("aaa",)


class ForTestingModerationViewSet(EssentialModuleViewSet):
    """
        Simple Viewset for testing purpose
    """

    permission_classes = tuple()
    serializer_class = ForTestingModerationSerializer
    queryset = ForTestingModeration.objects.all()
    end_point_route = "test/moderation"
