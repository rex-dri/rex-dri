import { setDisplayName } from "recompose";
import React, { useEffect } from "react";
import RequestParams from "../utils/api/RequestParams";
import Loading from "../components/common/Loading";
import useSingleApiData from "../hooks/wrappers/useSingleApiData";
import Counter from "../utils/counter";

/**
 * Simple class to the standardize the way data is requested.
 */
export class NetWrapParam {
  /**
   * @param {string} routeName - Route from where to get the data
   * @param {"all"|"one"} variant - Is it a "all" or a "one" (GET all or GET one object)
   * @param {string} [addDataToProp] - On what prop is the main data mapped
   * @param {RequestParams|function(props):RequestParams} [params] - RequestParams for the first request. If a function, takes as argument the props.
   * @param {Object} [propTypes] - Proptypes in case the param is a function using props.
   */
  constructor(
    routeName,
    variant,
    {
      addDataToProp = routeName,
      params = RequestParams.Builder.build(),
      // eslint-disable-next-line react/forbid-foreign-prop-types
      propTypes = {},
    } = {}
  ) {
    this.routeName = routeName;
    this.variant = variant;
    this.addDataToProp = addDataToProp;
    this.params = params;
    this.propTypes = propTypes;

    if (process.env.NODE_ENV !== "production") {
      if (typeof params === "function" && Object.keys(propTypes).length === 0) {
        // eslint-disable-next-line no-console
        console.error(params, propTypes);
        throw new Error(
          "propTypes optional argument must not be empty as params is a function taking the props of this component as argument."
        );
      }
    }
  }
}

/**
 * Helper to extract the data from the props based on a NetWrapParam instance
 * @param {Object} props
 * @param {NetWrapParam} netWrapParam
 * @returns {Object} -- sub object from props
 */
function extractProps(props, netWrapParam) {
  return Object.fromEntries(
    // eslint-disable-next-line react/forbid-foreign-prop-types
    Object.entries(props).filter(([key]) => key in netWrapParam.propTypes)
  );
}

/**
 * HOC Component to inject the data from the backend.
 *
 * An "api" prop is also injected to give access to setting the params, etc.
 *
 * @param {array.<NetWrapParam>} parameters
 * @param LoadingComponent
 * @return {function(*)}
 */
export default function withNetworkWrapper(
  parameters,
  LoadingComponent = Loading
) {
  if (process.env.NODE_ENV !== "production") {
    if (parameters.some((el) => !(el instanceof NetWrapParam))) {
      // eslint-disable-next-line no-console
      console.error(parameters);
      throw new Error(
        "Only NetWrapParam instances are accepted as parameter of withNetworkWrapper"
      );
    }
  }

  return (Component) =>
    setDisplayName("Network wrapper component")(
      // We need to forward the ref otherwise the styles are not correctly applied.
      // eslint-disable-next-line react/display-name
      React.forwardRef((props, ref) => {
        const allData = parameters.map((netWrapParam) => [
          useSingleApiData(netWrapParam.routeName, netWrapParam.variant, () =>
            typeof netWrapParam.params === "function"
              ? netWrapParam.params(extractProps(props, netWrapParam))
              : netWrapParam.params
          ),
          netWrapParam.addDataToProp,
        ]);
        const allHooks = allData.map((el) => el[0]);

        // eslint-disable-next-line no-plusplus
        parameters.forEach((netWrapParam, idx) => {
          if (typeof netWrapParam.params === "function") {
            // check if we automatically need to actualize
            const propsExtract = extractProps(props, netWrapParam);
            useEffect(() => {
              const associatedHook = allHooks[idx];
              associatedHook.setParams(netWrapParam.params(propsExtract));
            }, Object.values(propsExtract));
          }
        });

        // Error handling
        const hasError = allHooks.some((hook) => hook.hasError);
        if (hasError) return <div>Error</div>;

        // Loading handling
        const isLoading = allHooks.some((hook) => hook.isLoading);
        if (isLoading) return <LoadingComponent />;

        const newProps = { api: {} };
        allData.forEach(([hook, propKey]) => {
          // eslint-disable-next-line prefer-destructuring
          newProps[propKey] = hook.latestData;
          newProps.api[propKey] = hook;
        });

        if (process.env.NODE_ENV !== "production") {
          const newPropsKeys = parameters.map((p) => p.addDataToProp);
          newPropsKeys.push("api");

          const duplicatedKeys = new Counter(
            newPropsKeys
          ).getDuplicatedElements();

          if (duplicatedKeys.length !== 0) {
            // eslint-disable-next-line no-console
            console.warn(
              "Duplicated keys in withNetworkWrapper, this might result in inconsistencies.",
              duplicatedKeys,
              parameters
            );
          }
          newPropsKeys.forEach((propKey) => {
            if (propKey in props) {
              // eslint-disable-next-line no-console
              console.warn(
                `Props '${propKey}' already present in props. It's going to be overridden by the withNetworkWrapper HOC.`
              );
            }
          });
        }

        // Magically re-add proptypes in dev to make we are not missing anything
        if (process.env.NODE_ENV !== "production") {
          // eslint-disable-next-line react/forbid-foreign-prop-types
          const { propTypes = {} } = Component;
          // eslint-disable-next-line no-param-reassign
          Component.propTypes = {
            ...Object.assign(
              {},
              ...parameters.map((netWrapParams) => netWrapParams.propTypes)
            ),
            ...propTypes,
          };
        }

        return <Component {...props} {...newProps} ref={ref} />;
      })
    );
}
