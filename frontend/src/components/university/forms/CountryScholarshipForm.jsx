import React from "react";

import {
  ScholarshipFields,
  scholarshipFormLevelErrors,
} from "./common/Scholarship";
import { CountriesField } from "../../edition/fields/wrappedFields";
import FormInfo from "../../../utils/editionRelated/FormInfo";

export default new FormInfo(
  "countryScholarships",
  () => <ScholarshipFields LinkedField={CountriesField} />,
  scholarshipFormLevelErrors
);
