from django.db import models

from backend_app.models.abstract.base import BaseModel
from backend_app.models.university import University


class Partner(BaseModel):
    """
    Model that stores raw information from the UTC database regarding university partners.

    Some universities in UTC database are present multiple times. To accommodate for this,
    We store the data separetly and one REX-DRI university may link to multiple UTC partners.
    """

    # fields mapping directly to those of UTC db
    utc_id = models.IntegerField(primary_key=True)
    univ_name = models.CharField(max_length=80, null=False)
    address1 = models.CharField(max_length=100, null=True, blank=True)
    address2 = models.CharField(max_length=100, null=True, blank=True)
    zipcode = models.CharField(max_length=40, null=True)
    city = models.CharField(max_length=40, null=False)
    country = models.CharField(max_length=50, null=False)
    iso_code = models.CharField(max_length=2, null=False)
    is_destination_open = models.BooleanField(null=False, default=False)

    # field that will have to be set manually
    university = models.ForeignKey(
        University,
        null=True,
        on_delete=models.PROTECT,
        related_name="corresponding_utc_partners",
    )

    def save(self, *args, **kwargs):
        """
        Custom handling of denormalization
        """
        needs_to_propagate = False
        try:
            # it has already been saved to the db
            previous = Partner.objects.get(pk=self.pk)
            if (
                (previous.university is None and self.university is not None)
                or (previous.university is not None and self.university is None)
                or (
                    previous.university is not None
                    and self.university is not None
                    and previous.university.pk != self.university.pk
                )
            ):
                needs_to_propagate = True  # university has change for the partner
        except Partner.DoesNotExist:
            pass

        super().save(*args, **kwargs)

        if needs_to_propagate:
            from backend_app.models.exchange import Exchange
            from backend_app.models.offer import Offer

            for exchange in Exchange.objects.filter(utc_partner_id=self.utc_id):
                exchange.save()  # Trigger denormalisation update
            for offer in Offer.objects.filter(utc_partner_id=self.utc_id):
                offer.save()  # Trigger denormalisation update
