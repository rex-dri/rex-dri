import logging

from django.core.management.base import BaseCommand

from external_data.management.commands.utils import FixerData, UtcData

logger = logging.getLogger("django")


class Command(BaseCommand):
    help = "Command to handle updating remote data"

    def add_arguments(self, parser):
        subparsers = parser.add_subparsers(
            title="subcommands", dest="subcommand", required=False
        )
        subparsers.add_parser("all", help="(default) Update all external data")
        subparsers.add_parser("currencies", help="Update currencies from fixer")
        subparsers.add_parser("utc", help="Update the data from the UTC database")

    def handle(self, *args, **options):
        if "subcommand" in options.keys():
            subcommand = options["subcommand"]
            if subcommand == "all" or subcommand is None:
                self.update_all()
            elif options["subcommand"] == "currencies":
                self.update_currencies()
            elif options["subcommand"] == "utc":
                self.update_utc()
        else:
            self.update_all()

    @staticmethod
    def update_all():
        logger.info("Updating all external data")
        Command.update_currencies()
        Command.update_utc()

    @staticmethod
    def update_currencies():
        FixerData().update()

    @staticmethod
    def update_utc():
        UtcData().update()
