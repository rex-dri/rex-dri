import logging
import os

import requests
from django.db import transaction

from backend_app.models.course import Course
from backend_app.models.currency import Currency
from backend_app.models.exchange import Exchange, update_denormalized_univ_major_minor
from backend_app.models.offer import Offer
from backend_app.models.partner import Partner
from backend_app.models.university import update_denormalized_univ_field
from external_data.models import ExternalDataUpdateInfo

logger = logging.getLogger("django")

FETCHED_PAGE_SIZE = 500


def paged_data_walk(url, callback):
    """
    Helper function to fetch and parse paginated data from a paginated endpoint
    :param url: base endpoint location
    :param callback: function executed with the content of paginated response
    """
    current_page = 0
    while True:
        response = requests.get(
            "{}?page={}&size={}".format(url, current_page, FETCHED_PAGE_SIZE)
        )
        response_data = response.json()
        logger.info(
            "handling page {} out of {}".format(
                response_data["number"] + 1, response_data["totalPages"]
            )
        )
        callback(response_data["content"])
        if response_data["last"]:
            break
        else:
            current_page += 1


class FixerData(object):
    FIXER_API_TOKEN = os.environ["FIXER_API_TOKEN"].strip()

    def update(self):
        logger.info("Updating currencies from Fixer API")

        with transaction.atomic():
            response = requests.get(
                "http://data.fixer.io/api/latest?access_key={}".format(
                    self.FIXER_API_TOKEN
                )
            )

            data = response.json()

            if data["success"]:
                for (code, rate) in response.json()["rates"].items():
                    Currency.objects.update_or_create(
                        code=code, defaults={"one_EUR_in_this_currency": rate}
                    )
                ExternalDataUpdateInfo.objects.create(source="fixer")
                logger.info("Currency update was successful")

            else:
                logger.error(
                    "Updating currency information from fixer failed, see response from the API below."
                )
                logger.error(response.json())


class UtcData(object):
    UTC_API_ENDPOINT = os.environ["UTC_API_ENDPOINT"].strip()

    def __init__(self):
        self.all_exchanges_utc_id_from_ent = set()
        self.all_courses_utc_id_from_ent = set()

    def update(self):
        logger.info("Updating UTC Data")

        with transaction.atomic():
            self.update_partners()

            # Reset destination open status on all partners
            # before fetching open destinations
            Partner.objects.all().update(is_destination_open=False)
            paged_data_walk(
                "{}/openedDestinations".format(self.UTC_API_ENDPOINT),
                self.__import_opened_destinations,
            )
            paged_data_walk(
                "{}/exchanges".format(self.UTC_API_ENDPOINT),
                self.__import_exchange_data,
            )
            paged_data_walk(
                "{}/courses".format(self.UTC_API_ENDPOINT), self.__import_course_data
            )
            ExternalDataUpdateInfo.objects.create(source="utc")
            logger.info("Updating invalidated")
            self.__update_invalidated()
            logger.info("Updating UTC info done !")

        update_denormalized_univ_major_minor()
        update_denormalized_univ_field()

    def __update_invalidated(self):
        """
        Function to update the unlinked status of exchanges and courses.
        It must be run after update.
        """
        for exchange in Exchange.objects.all():
            if exchange.utc_id not in self.all_exchanges_utc_id_from_ent:
                exchange.unlinked = True
                exchange.save()

        for course in Course.objects.all():
            if course.utc_id not in self.all_courses_utc_id_from_ent:
                course.unlinked = True
                course.save()

    def update_partners(self):
        paged_data_walk(
            "{}/universities".format(self.UTC_API_ENDPOINT),
            self.__import_utc_partner_data,
        )

        if Partner.objects.filter(university=None).count() > 0:
            logger.warning(
                "There are UTC partners that are not mapped to REX-DRI university"
            )

    @classmethod
    def get_local_partners(cls):
        return set(map(lambda p: p.pk, Partner.objects.all()))

    def update_one_student(self, student_utc_login):
        local_partners = self.get_local_partners()

        response_exchange = requests.get(
            "{}/exchanges/{}".format(self.UTC_API_ENDPOINT, student_utc_login)
        ).json()

        for exchange in response_exchange:
            exchange_id = exchange["idDepart"]
            partner_id = exchange["idEtab"]
            if partner_id not in local_partners:
                # An update of the partners in REX-DRI db is also needed
                self.update_partners()
                local_partners = self.get_local_partners()
                if partner_id not in local_partners:
                    raise Exception(
                        "Unknown partner for an exchange {}".format(exchange_id)
                    )

        self.__import_exchange_data(response_exchange)

        response_courses = requests.get(
            "{}/courses/{}".format(self.UTC_API_ENDPOINT, student_utc_login)
        ).json()
        self.__import_course_data(response_courses)

    @classmethod
    def __import_utc_partner_data(cls, response_content):
        logger.info("Importing university partners")

        for univ in response_content:
            logger.debug(univ["idEtab"])

            Partner.objects.update_or_create(
                utc_id=univ["idEtab"],
                defaults=dict(
                    univ_name=univ["nomEtab"],
                    address1=univ["adr1"],
                    address2=univ["adr2"],
                    zipcode=univ["codePostal"],
                    city=univ["ville"],
                    country=univ["pays"],
                    iso_code=univ["codeIso"],
                ),
            )

    def __import_exchange_data(self, response_content):
        logger.info("Importing exchange data")

        for exchange in response_content:
            exchange_id = exchange["idDepart"]
            self.all_exchanges_utc_id_from_ent.add(exchange_id)
            logger.debug("Exchange id: {}".format(exchange_id))

            Exchange.objects.update_or_create(
                utc_id=exchange_id,
                defaults=dict(
                    utc_partner_id=exchange["idEtab"],
                    semester=exchange["semestreDepart"][0],
                    year=exchange["semestreDepart"][1:],
                    duration=exchange["duree"],
                    double_degree=exchange["doubleDiplome"],
                    master_obtained=exchange["master"],
                    student_major_and_semester=exchange["specialite"],
                    student_minor=exchange["option"],
                    utc_allow_courses=exchange["autorisationTransfertUv"],
                    utc_allow_login=exchange["autorisationTransfertLogin"],
                    unlinked=False,
                ),
            )

    @classmethod
    def __import_opened_destinations(cls, response_content):
        logger.info("Importing the list of opened destinations")

        for destination in response_content:
            utc_partner_id = destination["idEtab"]
            logger.info(utc_partner_id)

            semester = destination["semestre"][:1]
            year = destination["semestre"][1:]

            Offer.objects.update_or_create(
                utc_partner_id=utc_partner_id,
                year=year,
                semester=semester,
                defaults=dict(
                    comment=destination["commentaire"],
                    nb_seats_offered=destination["nombrePlaces"],
                    double_degree=destination["doubleDiplome"],
                    is_master_offered=destination["master"],
                    majors=destination["branches"],
                ),
            )
            # Don't forget to update the destination open status
            Partner.objects.filter(utc_id=utc_partner_id).update(
                is_destination_open=True
            )

    def __import_course_data(self, response_content):
        logger.info("Importing courses data")

        for course in response_content:
            course_utc_id = course["idCours"]
            self.all_courses_utc_id_from_ent.add(course_utc_id)
            Course.objects.update_or_create(
                utc_id=course_utc_id,
                defaults=dict(
                    utc_exchange_id=course["idDepart"],
                    code=course["code"],
                    title=course["titre"],
                    link=course["lien"],
                    ects=course["ects"],
                    category=course["categorie"],
                    profile=course["profil"],
                    tsh_profile=course["categorieTsh"],
                    student_login=course["login"],
                    unlinked=False,
                ),
            )
