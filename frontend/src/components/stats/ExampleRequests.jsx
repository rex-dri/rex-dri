import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  container: { display: "flex", alignItems: "center" },
  button: {
    marginRight: theme.spacing(1),
    marginTop: theme.spacing(0.5),
    marginBottom: theme.spacing(0.5),
    minWidth: "auto",
  },
}));

/**
 * Component to display information about the dataset
 */
function ExampleRequests({ requests, performRequest }) {
  const classes = useStyles();

  return (
    <>
      {requests.map(({ label, request }) => (
        <div key={label} className={classes.container}>
          <Button
            variant="outlined"
            onClick={() => performRequest(request)}
            size="small"
            className={classes.button}
          >
            Exécuter
          </Button>
          <Typography>{label}</Typography>
        </div>
      ))}
    </>
  );
}

ExampleRequests.propTypes = {
  requests: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      request: PropTypes.string.isRequired,
    })
  ).isRequired,
  performRequest: PropTypes.func.isRequired,
};

export default React.memo(ExampleRequests);
