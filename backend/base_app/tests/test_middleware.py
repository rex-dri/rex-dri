from django.test import TestCase
from rest_framework.test import APIClient


class MiddlewareAuthorizedMethodsTestCase(TestCase):
    """
    Test of REX-DRI middleware.
    Part regarding authorized HTTP methods
    """

    url = "/admin/login/"

    def test_get_allowed(self):
        client = APIClient()
        response = client.get(self.url)
        self.assertNotEqual(response.status_code, 201)

    def test_patch_not_allowed(self):
        client = APIClient()
        response = client.patch(self.url)
        self.assertEqual(response.status_code, 401)
