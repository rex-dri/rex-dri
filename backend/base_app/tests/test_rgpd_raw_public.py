from django.test import TestCase
from rest_framework.test import APIClient


class HasAccessTestCase(TestCase):
    """
    Test to check that the RGPD doc is publicly accessible
    """

    def test_unauthentificated_can_access(self):
        client = APIClient()
        response = client.get("/rgpd-raw/")
        self.assertEqual(response.status_code, 200)
