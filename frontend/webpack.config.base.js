const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BundleTracker = require("webpack-bundle-tracker");
const CopyPlugin = require("copy-webpack-plugin");

const config = {
  entry: {
    main: ["./src/entry/mainApp.jsx"],
    stats: ["./src/entry/statsApp.jsx"],
    rgpdCgu: ["./src/entry/rgpdCguForm.jsx"],
    rgpdRaw: ["./src/entry/rgpdRaw.jsx"],
  },
  output: {
    path: __dirname + "/dist/bundles",
    filename: "[name]-[hash].js",
  },
  resolve: {
    extensions: ["*", ".json", ".js", ".jsx"],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          compact: true,
        },
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it use publicPath in webpackOptions.output
              publicPath: "../",
            },
          },
          "css-loader",
          "sass-loader",
        ],
      },
      {
        test: /\.yml/,
        use: "js-yaml-loader",
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              publicPath: "./fonts/",
              outputPath: "fonts/",
            },
          },
        ],
      },
      {
        test: /\.(md)$/,
        use: "raw-loader",
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name]-[hash].css",
      chunkFilename: "[id]-[hash].css",
    }),
    new CopyPlugin([
      { from: "node_modules/mapbox-gl/dist", to: "mapbox-gl-dist" },
    ]),
    new BundleTracker({ filename: "./webpack-stats.json" }),
  ],
};

module.exports = config;
