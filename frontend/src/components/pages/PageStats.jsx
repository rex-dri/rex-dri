import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import Divider from "@material-ui/core/Divider";
import { withPaddedPaper } from "./shared";
import RequestInterface from "../stats/RequestInterface";
import DatasetSelector from "../stats/DatasetSelector";
import DatasetDescriptor from "../stats/DatasetDescriptor";
import {
  availableDatasets,
  changeDataset,
  currentDatasetName,
  getDatasetColumns,
} from "../stats/utils";
import PickDataDateRange from "../stats/PickDataDateRange";

const useStyles = makeStyles((theme) => ({
  datasetTabBar: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  requestInterface: {},
  spacer: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

/**
 * Component corresponding to the stats page of the site
 */
function PageStats() {
  const classes = useStyles();
  const datasetInfo = availableDatasets.filter(
    (el) => el.name === currentDatasetName
  )[0].info;

  return (
    <>
      <Typography variant="h4">
        Exploration des statistiques d'utilisation du site REX-DRI
      </Typography>
      <div className={classes.datasetTabBar}>
        <DatasetSelector
          datasets={availableDatasets}
          onChange={changeDataset}
          value={currentDatasetName}
        />
      </div>
      <PickDataDateRange />
      <Divider className={classes.spacer} />
      <DatasetDescriptor
        columns={getDatasetColumns(currentDatasetName)}
        info={datasetInfo || ""}
      />
      <Divider className={classes.spacer} />
      <div className={classes.requestInterface}>
        <RequestInterface />
      </div>
    </>
  );
}

PageStats.propTypes = {};

export default withPaddedPaper()(PageStats);
