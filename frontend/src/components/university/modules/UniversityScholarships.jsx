import React, { useMemo } from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";

import { makeStyles } from "@material-ui/styles";
import ModuleWrapper from "./common/ModuleWrapper";
import ModuleGroupWrapper from "./common/ModuleGroupWrapper";

import UniversityScholarshipForm from "../forms/UniversityScholarshipForm";
import RequestParams from "../../../utils/api/RequestParams";
import withUnivInfo from "../../../hoc/withUnivInfo";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";
import ScholarshipCore, {
  getScholarshipDefaultModelData,
} from "./common/ScholarshipCore";

const useStyles = makeStyles((theme) => ({
  item: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
}));

function UniversityScholarships({ univId, univScholarshipsItems }) {
  const classes = useStyles();

  const defaultModelData = useMemo(
    () => ({
      ...getScholarshipDefaultModelData(),
      universities: [univId],
    }),
    [univId]
  );

  return (
    <ModuleGroupWrapper
      groupTitle={"Bourses liées à l'université"}
      formInfo={UniversityScholarshipForm}
      defaultModelData={defaultModelData}
    >
      {univScholarshipsItems.map((rawModelData) => (
        <div key={rawModelData.id} className={classes.item}>
          <ModuleWrapper
            buildTitle={(modelData) => modelData.title}
            rawModelData={rawModelData}
            formInfo={UniversityScholarshipForm}
            CoreComponent={ScholarshipCore}
          />
        </div>
      ))}
    </ModuleGroupWrapper>
  );
}

UniversityScholarships.propTypes = {
  univId: PropTypes.number.isRequired,
  univScholarshipsItems: PropTypes.array.isRequired,
};

const buildParams = (univId) =>
  RequestParams.Builder.withQueryParam("universities", univId).build();

export default compose(
  withUnivInfo(),
  withNetworkWrapper([
    new NetWrapParam("universityScholarships", "all", {
      addDataToProp: "univScholarshipsItems",
      params: (props) => buildParams(props.univId),
      propTypes: {
        univId: PropTypes.number.isRequired,
      },
    }),
  ])
)(UniversityScholarships);
