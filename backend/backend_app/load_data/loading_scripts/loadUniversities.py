from os.path import abspath, join

from backend_app.load_data.utils import ASSETS_PATH, csv_2_dict_list
from backend_app.models.country import Country
from backend_app.models.partner import Partner
from backend_app.models.university import University
from base_app.models import User
from .loadGeneric import LoadGeneric


class LoadUniversities(LoadGeneric):
    """
    Load the universities in the app
    """

    def __init__(self, admin: User):
        self.admin = admin

    @staticmethod
    def get_destination_data():
        destinations_path = abspath(join(ASSETS_PATH, "destinations_extracted.csv"))
        return csv_2_dict_list(destinations_path)

    def load(self):
        for row in self.get_destination_data():
            lat = round(float(row["lat"]), 6)
            lon = round(float(row["lon"]), 6)

            country = Country.objects.get(pk=row["country"])

            univ = University.objects.update_or_create(
                pk=row["utc_id"],  # Not perfect but should do the trick
                defaults={
                    "name": row["university"],
                    "acronym": row["acronym"],
                    "website": row["website"],
                    "city": row["city"],
                    "country": country,
                    "main_campus_lat": lat,
                    "main_campus_lon": lon,
                    # "logo": row["logo"],  # WARNING FIX BETA not ok
                },
            )[0]
            self.add_info_and_save(univ, self.admin)

            Partner.objects.update_or_create(
                utc_id=row["utc_id"],
                defaults=dict(
                    univ_name=row["university"],
                    city=row["city"],
                    country=row["country"],
                    iso_code=row["country"],
                    university=univ,
                ),
            )
