import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Pagination from "@material-ui/lab/Pagination";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 800,
    margin: "0 auto",
  },
  stepperContainer: {
    margin: "0 auto",
    marginBottom: theme.spacing(1),
    width: "max-content",
  },
  progress: {
    [theme.breakpoints.up("sm")]: {
      width: "65%",
    },
    [theme.breakpoints.down("sm")]: {
      width: "60%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "30%",
    },
  },
}));

function PaginatedData(props) {
  const classes = useStyles();
  const {
    data,
    goToPage,
    render,
    stepperOnBottom,
    stepperOnTop,
    EmptyMessageComponent,
  } = props;
  const {
    page,
    pages_count: nbOfPages,
    content,
    number_elements: totalElements,
  } = data;

  if (totalElements === 0) {
    return EmptyMessageComponent;
  }

  const handleChange = (event, value) => {
    goToPage(value);
  };

  function renderStepper() {
    if (nbOfPages === 1) {
      return <></>;
    }
    return (
      <div className={classes.stepperContainer}>
        <Pagination
          count={nbOfPages}
          page={page}
          onChange={handleChange}
          showFirstButton
          showLastButton
          color="secondary"
        />
      </div>
    );
  }

  return (
    <div>
      {stepperOnTop && renderStepper()}
      <div>{content.map((dataEl) => render(dataEl))}</div>
      {stepperOnBottom && renderStepper()}
    </div>
  );
}

PaginatedData.defaultProps = {
  stepperOnTop: false,
  stepperOnBottom: false,
};

PaginatedData.propTypes = {
  data: PropTypes.shape({
    first: PropTypes.bool.isRequired,
    last: PropTypes.bool.isRequired,
    page: PropTypes.number.isRequired,
    pages_count: PropTypes.number.isRequired,
    number_elements: PropTypes.number.isRequired,
    page_size: PropTypes.number.isRequired,
    content: PropTypes.array.isRequired,
  }).isRequired,
  goToPage: PropTypes.func.isRequired,
  render: PropTypes.func.isRequired,
  stepperOnTop: PropTypes.bool,
  stepperOnBottom: PropTypes.bool,
  EmptyMessageComponent: PropTypes.node.isRequired,
};

export default PaginatedData;
