version: "3.6"

networks:
  backend-db:
  map-nginx:

volumes:
  postgres_data_prod:
  media_files:
  uwsgi_socket_dir:
  django_logs:
  frontend_logs:
  nginx_logs:

services:
  backend:
    image: registry.gitlab.utc.fr/rex-dri/rex-dri/backend-prod:v0.2.1
    restart: on-failure
    volumes:
      - ../:/usr/src/app/
      - media_files:/usr/src/app/backend/media
      - uwsgi_socket_dir:/usr/src/socket/
      - ./uwsgi/:/usr/src/uwsgi/:ro
      - django_logs:/var/log/django
      - frontend_logs:/var/log/frontend
    env_file: [./envs/db.env, ./envs/django.env, ./envs/external_data.env]
    environment:
      WAIT_HOSTS: database:5432 # For the 'wait' script, so that we are sure the db is up and running
      ENV: PROD # make sure to be in prod env
    # we don't forget to have the spooler dir created on start
    command: /bin/sh -c "rm -rf /usr/src/spooler && mkdir /usr/src/spooler && /wait && cd backend && ./entry.sh && uwsgi --log-master --ini /usr/src/uwsgi/uwsgi.ini"
    networks: [backend-db]
    depends_on: [database, frontend] # - frontend

  database:
    image: postgres:10.5-alpine
    networks: [backend-db]
    env_file: [./envs/db.env]
    volumes: ["postgres_data_prod:/var/lib/postgresql/data/"]

  frontend: # Will be killed as soon as the front is generated
    image: registry.gitlab.utc.fr/rex-dri/rex-dri/frontend:v2.1.1
    command: /bin/sh -c "cd frontend && mv -f /usr/src/deps/node_modules/* /usr/src/deps/node_modules/.bin ./node_modules/ && yarn build"
    networks: []
    volumes:
      - ../:/usr/src/app/
      - /usr/src/app/frontend/node_modules

  nginx:
    build:
      context: ./nginx
      dockerfile: prod.Dockerfile
    restart: always
    volumes:
      - uwsgi_socket_dir:/usr/src/socket/
      - ../backend/static/:/usr/src/static:ro
      - media_files:/usr/src/media:ro
      - nginx_logs:/var/log/nginx
    ports: ["80:80"]
    networks: [map-nginx]
    depends_on: [backend, map]
    command: [nginx-debug, "-g", "daemon off;"]

  map:
    image: "floawfloaw/light-world-tileserver:2019-05-21--zoom-${MAP_ZOOM_LEVEL}"
    networks: [map-nginx]
    volumes: ["./map:/data/custom:ro"]
    entrypoint:
      [
        "node",
        "/usr/src/app/",
        "-p",
        "8080",
        "--config",
        "/data/custom/config.json",
        "--public_url",
        "${MAP_PUBLIC_URL}",
        "--silent",
      ]
    restart: always

  logs_rotation:
    image: blacklabelops/logrotate:1.3
    environment:
      LOGS_DIRECTORIES: /var/log/django /var/log/nginx /var/log/frontend
      LOGROTATE_INTERVAL: daily
      LOGROTATE_COPIES: 30
      LOGROTATE_COMPRESSION: compress
    networks: []
    volumes:
      - django_logs:/var/log/django
      - frontend_logs:/var/log/frontend
      - nginx_logs:/var/log/nginx
    depends_on:
      - nginx
      - backend
